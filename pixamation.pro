TEMPLATE = subdirs

SUBDIRS += \
    pixamation \
    pixamation-app \
    pixamation-tests

pixamation-app.depends = pixamation
pixamation-tests.depends = pixamation
