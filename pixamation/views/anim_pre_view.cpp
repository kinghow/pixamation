#include "anim_pre_view.h"
#include <QScrollBar>
#include <QPainter>
#include <QPaintEvent>


AnimPreView::AnimPreView(QWidget *parent)
    : QAbstractItemView(parent)
    , m_zoom(1.f)
{
    setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));
    setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
    setEditTriggers(QAbstractItemView::NoEditTriggers);
    setSelectionMode(QAbstractItemView::NoSelection);
    setAutoScroll(false);

    horizontalScrollBar()->setRange(0, 0);
    verticalScrollBar()->setRange(0, 0);
}


void AnimPreView::setZoom(float factor)
{
    m_zoom = factor;
    viewport()->update();
    updateGeometries();
}


void AnimPreView::nextIndex()
{
    if (currentIndex().row() != model()->rowCount() - 1)
        setCurrentIndex(model()->index(currentIndex().row() + 1, 0));
}


void AnimPreView::prevIndex()
{
    if (currentIndex().row() != 0)
        setCurrentIndex(model()->index(currentIndex().row() - 1, 0));
}


void AnimPreView::firstIndex() { setCurrentIndex(model()->index(0, 0)); }


void AnimPreView::lastIndex() { setCurrentIndex(model()->index(model()->rowCount() - 1, 0)); }


void AnimPreView::onTick()
{
    if (currentIndex().row() == model()->rowCount() - 1)
        setCurrentIndex(model()->index(0, 0));
    else
        setCurrentIndex(model()->index(currentIndex().row() + 1, 0));
}


// Items occupy whole viewport.
QRect AnimPreView::visualRect(const QModelIndex &) const
{
    QRect rect = viewport()->rect();
    rect.translate(-horizontalOffset(), -verticalOffset());

    return rect;
}


// Not used.
void AnimPreView::scrollTo(const QModelIndex &, ScrollHint) {}


// Not used.
QModelIndex AnimPreView::indexAt(const QPoint &) const { return QModelIndex(); }


// Not used.
QModelIndex AnimPreView::moveCursor(CursorAction, Qt::KeyboardModifiers) { return QModelIndex(); }


int AnimPreView::horizontalOffset() const { return horizontalScrollBar()->value(); }


int AnimPreView::verticalOffset() const { return verticalScrollBar()->value(); }


// No hidden items.
bool AnimPreView::isIndexHidden(const QModelIndex &) const { return false; }


// No selection.
void AnimPreView::setSelection(const QRect &, QItemSelectionModel::SelectionFlags) {}


// No selection.
QRegion AnimPreView::visualRegionForSelection(const QItemSelection &) const{ return QRegion(); }


void AnimPreView::dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight,
                              const QList<int> &roles)
{
    QAbstractItemView::dataChanged(topLeft, bottomRight, roles);
    viewport()->update();
}


void AnimPreView::updateGeometries()
{
    if (!model())
        return;

    // Enable scrollbars when viewport is too small.
    QSize itemSize = model()->index(0, 0).data().value<QPixmap>().size() * m_zoom;

    horizontalScrollBar()->setPageStep(viewport()->width());
    horizontalScrollBar()->setRange(0, std::max(0, itemSize.width() - viewport()->width()));
    verticalScrollBar()->setPageStep(viewport()->height());
    verticalScrollBar()->setRange(0, std::max(0, itemSize.height() - viewport()->height()));
}


void AnimPreView::paintEvent(QPaintEvent *event)
{
    if (!model())
        return;

    QPainter painter(viewport());
    // Factor in scroll offsets.
    painter.translate(-horizontalOffset(), -verticalOffset());

    QStyleOptionViewItem option;
    initViewItemOption(&option);
    option.rect = event->rect();


    // Paint current frame.
    QPixmap current = currentIndex().data().value<QPixmap>();
    QPixmap zoomed = current.scaled(current.size() * m_zoom);
    QPoint topLeft(std::max(0, option.rect.center().x() - zoomed.size().width() / 2),
                   std::max(0, option.rect.center().y() - zoomed.size().height() / 2));
    QRect centered(topLeft, zoomed.size());
    painter.drawPixmap(centered, zoomed);


    itemDelegate()->paint(&painter, option, currentIndex());
}


void AnimPreView::resizeEvent(QResizeEvent *) { updateGeometries(); }


void AnimPreView::scrollContentsBy(int dx, int dy)
{
    viewport()->scroll(dx, dy);
    viewport()->update();
}
