#ifndef CANVASVIEW_H
#define CANVASVIEW_H

#include <QAbstractItemView>
#include <QPixmap>


///
/// \brief Main display for frame editing. The canvas view usually displays 1 item only but can
/// also display multiple items on top of one another (onion skinning).
///
/// Call setCanvasBackground before using the canvas.
///
class CanvasView : public QAbstractItemView
{
    Q_OBJECT
public:
    CanvasView(QWidget *parent = nullptr);
    void setCanvasBackground(QPixmap backgroundPattern);

public slots:
    void setZoom(float factor);
    void updateViewport();

private:
    float m_zoom;
    QPixmap m_background;


    // QAbstractItemView interface
public:
    virtual QRect visualRect(const QModelIndex &) const override;
    virtual void scrollTo(const QModelIndex &, ScrollHint) override;
    virtual QModelIndex indexAt(const QPoint &) const override;
protected:
    virtual QModelIndex moveCursor(CursorAction, Qt::KeyboardModifiers) override;
    virtual int horizontalOffset() const override;
    virtual int verticalOffset() const override;
    virtual bool isIndexHidden(const QModelIndex &) const override;
    virtual void setSelection(const QRect &, QItemSelectionModel::SelectionFlags) override;
    virtual QRegion visualRegionForSelection(const QItemSelection &) const override;
protected slots:
    virtual void dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight,
                             const QList<int> &roles) override;
    virtual void updateGeometries() override;

    // QAbstractScrollArea interface
protected:
    virtual void scrollContentsBy(int dx, int dy) override;

    // QWidget interface
protected:
    virtual void paintEvent(QPaintEvent *event) override;
    virtual void resizeEvent(QResizeEvent *) override;
    virtual void mousePressEvent(QMouseEvent *event) override;
    virtual void mouseMoveEvent(QMouseEvent *) override;
};


#endif // CANVASVIEW_H
