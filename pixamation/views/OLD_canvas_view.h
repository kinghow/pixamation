#ifndef CANVASVIEW_H
#define CANVASVIEW_H

#include <QListView>


class CanvasView : public QListView
{
    Q_OBJECT
public:
    CanvasView(QWidget *parent = nullptr);

public slots:
    void onZoomChanged();


    // QWidget interface
protected:
    virtual void mouseMoveEvent(QMouseEvent *) override;
    virtual void resizeEvent(QResizeEvent *event) override;

    // QAbstractItemView interface
protected slots:
    virtual void closeEditor(QWidget *, QAbstractItemDelegate::EndEditHint) override;
    virtual void scrollTo(const QModelIndex &, QAbstractItemView::ScrollHint hint) override;
};


#endif // CANVASVIEW_H
