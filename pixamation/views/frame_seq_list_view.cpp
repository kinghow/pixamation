#include "frame_seq_list_view.h"
#include <QHBoxLayout>
#include <QPushButton>
#include <QDropEvent>
#include "commands/command_stack.h"
#include "commands/insert_frame_command.h"
#include "commands/remove_frame_command.h"
#include "commands/move_frame_command.h"


FrameSeqListView::FrameSeqListView(QWidget *parent)
    : QListView(parent)
{
    setSelectionMode(QAbstractItemView::ExtendedSelection);
    setDragEnabled(true);
    setAcceptDrops(true);
    setDropIndicatorShown(true);
    setDragDropMode(QAbstractItemView::InternalMove);
    setFlow(QListView::LeftToRight);
    setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));
    setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
    setEditTriggers(QAbstractItemView::NoEditTriggers);
    setDragDropOverwriteMode(true);
}


void FrameSeqListView::insertRows()
{
    if (!model())
        return;

    InsertFrameCommand *cmd = new InsertFrameCommand(static_cast<FrameSeqModel *>(model()),
                                                     selectionModel());
    CommandStack::stack->push(cmd);
}


void FrameSeqListView::removeRows()
{
    if (!model())
        return;

    // Keep 1 minimum item invariant.
    if (model()->rowCount() == 1)
        return;

    RemoveFrameCommand *cmd = new RemoveFrameCommand(static_cast<FrameSeqModel *>(model()),
                                                     selectionModel());
    CommandStack::stack->push(cmd);
}


void FrameSeqListView::currentChanged(const QModelIndex &current, const QModelIndex &previous)
{
    QListView::currentChanged(current, previous);
    emit currentIndexChanged(current);
}


QSize FrameSeqListView::sizeHint() const { return { QListView::sizeHint().width(), 140 }; }


void FrameSeqListView::dropEvent(QDropEvent *event)
{
    if (event->dropAction() == Qt::MoveAction || dragDropMode() == QAbstractItemView::InternalMove)
    {
        // Check for contiguity in the selection.
        auto selection = selectionModel()->selectedIndexes();
        if (selection.size() == 0)
            return;
        std::sort(selection.begin(), selection.end());

        bool isContiguous = true;
        int currentRow = selection.begin()->row();
        for (const auto &index : selection)
        {
            if (currentRow != index.row())
            {
                isContiguous = false;
                break;
            }
            ++currentRow;
        }


        // Get destination row.
        QModelIndex dropIndex = indexAt(event->position().toPoint());
        int beginRow = 0;
        switch (dropIndicatorPosition())
        {
        case QAbstractItemView::AboveItem:
            if (isContiguous)
            {
                // Placing contiguous selection at the beginning of the selection does nothing.
                if (dropIndex.row() == selection.last().row() + 1)
                    return;
                // Selection is already at the beginning.
                if (dropIndex.row() == selection.first().row())
                    return;
            }
            beginRow = dropIndex.row();
            break;

        case QAbstractItemView::BelowItem:
            // Placing contiguous selection at the end of the selection does nothing.
            if (isContiguous)
                if (dropIndex.row() == selection.first().row() - 1)
                    return;
            beginRow = dropIndex.row() + 1;
            break;

        case QAbstractItemView::OnViewport:
            if (isContiguous)
            {
                // Everything is selected. Nothing should be moved.
                if (selection.size() == model()->rowCount())
                    return;
                // Selection is already at the end.
                if (selection.back().row() == model()->rowCount() - 1)
                    return;
            }
            beginRow = model()->rowCount();
            break;

        // Disabled via model.
        case QAbstractItemView::OnItem:
            return;
        }


        MoveFrameCommand *cmd = new MoveFrameCommand(static_cast<FrameSeqModel *>(model()),
                                                     selectionModel(), beginRow);
        CommandStack::stack->push(cmd);
    }
}
