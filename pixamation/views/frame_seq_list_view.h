#ifndef FRAMESEQLISTVIEW_H
#define FRAMESEQLISTVIEW_H

#include <QListView>


///
/// \brief Displays a model as a list. Supports bulk insertion or removal based on the current
/// selections. Insertions and removals do nothing without a connected a model. Also supports
/// rearranging items via drag and drop.
///
class FrameSeqListView : public QListView
{
    Q_OBJECT
public:
    FrameSeqListView(QWidget *parent = nullptr);

signals:
    void currentIndexChanged(const QModelIndex &index);

public slots:
    // Insert and remove based on the current selection/index.
    void insertRows();
    void removeRows();


    // QAbstractItemView interface
protected slots:
    virtual void currentChanged(const QModelIndex &current, const QModelIndex &previous) override;

    // QWidget interface
protected:
    virtual QSize sizeHint() const override;
    virtual void dropEvent(QDropEvent *event) override;
};


#endif // FRAMESEQLISTVIEW_H
