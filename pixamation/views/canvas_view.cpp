#include "canvas_view.h"

#include <QScrollBar>
#include <QPainter>
#include <QPaintEvent>
#include "models/frame_seq_canvas_proxy.h"


CanvasView::CanvasView(QWidget *parent)
    : QAbstractItemView(parent)
    , m_zoom(1.f)
    , m_background(1, 1)
{
    setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));
    setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    setEditTriggers(QAbstractItemView::NoEditTriggers);
    setSelectionMode(QAbstractItemView::NoSelection);
    setAutoScroll(false);

    horizontalScrollBar()->setRange(0, 0);
    verticalScrollBar()->setRange(0, 0);
    setMouseTracking(true);
}


void CanvasView::setCanvasBackground(QPixmap backgroundPattern)
{
    assert(model() != nullptr && "setCanvasBackground called before setting model.");

    m_background = m_background.scaled(model()->index(0, 0).data().value<QPixmap>().size());
    QPainter painter(&m_background);
    painter.fillRect(m_background.rect(), QBrush(backgroundPattern));
}


void CanvasView::setZoom(float factor)
{
    m_zoom = factor;
    viewport()->update();
    updateGeometries();
}


void CanvasView::updateViewport() { viewport()->update(); }


// Canvas items occupy the whole viewport.
QRect CanvasView::visualRect(const QModelIndex &) const
{
    QRect rect = viewport()->rect();
    rect.translate(-horizontalOffset(), -verticalOffset());

    return rect;
}


// For programmatic scrolling.
void CanvasView::scrollTo(const QModelIndex &, ScrollHint)
{
    QRect area = viewport()->rect();
    QRect rect = visualRect(QModelIndex());

    if (rect.left() < area.left())
        horizontalScrollBar()->setValue(horizontalOffset() +rect.left() - area.left());
    else if (rect.right() > area.right())
        horizontalScrollBar()->setValue(horizontalOffset() + std::min(rect.right() - area.right(),
                                                                      rect.left() - area.left()));

    if (rect.top() < area.top())
        verticalScrollBar()->setValue(verticalOffset() + rect.top() - area.top());
    else if (rect.bottom() > area.bottom())
        verticalScrollBar()->setValue(verticalOffset() + std::min(rect.bottom() - area.bottom(),
                                                                  rect.top() - area.top()));

    // Refresh the editor when scrolled.
    if (model())
    {
        FrameSeqCanvasProxy *proxy = static_cast<FrameSeqCanvasProxy *>(model());
        QWidget* editor = indexWidget(proxy->currentIndex());
        if (editor != nullptr)
        {
            closeEditor(editor, QAbstractItemDelegate::NoHint);
            edit(proxy->currentIndex());
        }
    }

    viewport()->update();
}


// Return only current item.
QModelIndex CanvasView::indexAt(const QPoint &) const
{
    if (!model())
        return QModelIndex();

    FrameSeqCanvasProxy *proxy = static_cast<FrameSeqCanvasProxy *>(model());
    return proxy->currentIndex();
}


// Return only current item.
QModelIndex CanvasView::moveCursor(CursorAction, Qt::KeyboardModifiers)
{
    if (!model())
        return QModelIndex();

    FrameSeqCanvasProxy *proxy = static_cast<FrameSeqCanvasProxy *>(model());
    return proxy->currentIndex();
}


int CanvasView::horizontalOffset() const { return horizontalScrollBar()->value(); }


int CanvasView::verticalOffset() const { return verticalScrollBar()->value(); }


// No hidden items.
bool CanvasView::isIndexHidden(const QModelIndex &) const { return false; }


// No selection.
void CanvasView::setSelection(const QRect &, QItemSelectionModel::SelectionFlags) {}


// No selection.
QRegion CanvasView::visualRegionForSelection(const QItemSelection &) const{ return QRegion(); }


void CanvasView::dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight,
                             const QList<int> &roles)
{
    QAbstractItemView::dataChanged(topLeft, bottomRight, roles);
    viewport()->update();
}


void CanvasView::updateGeometries()
{
    if (!model())
        return;

    // Enable scrollbars when viewport is too small.
    QSize itemSize = model()->index(0, 0).data().value<QPixmap>().size() * m_zoom;

    horizontalScrollBar()->setPageStep(viewport()->width());
    horizontalScrollBar()->setRange(0, std::max(0, itemSize.width() - viewport()->width()));
    verticalScrollBar()->setPageStep(viewport()->height());
    verticalScrollBar()->setRange(0, std::max(0, itemSize.height() - viewport()->height()));

    // Refresh the editor when scrolled.
    if (model())
    {
        FrameSeqCanvasProxy *proxy = static_cast<FrameSeqCanvasProxy *>(model());
        QWidget* editor = indexWidget(proxy->currentIndex());
        if (editor != nullptr)
        {
            closeEditor(editor, QAbstractItemDelegate::NoHint);
            edit(proxy->currentIndex());
        }
    }
}


void CanvasView::scrollContentsBy(int dx, int dy)
{
    viewport()->scroll(dx, dy);

    // Refresh the editor when scrolled.
    if (model())
    {
        FrameSeqCanvasProxy *proxy = static_cast<FrameSeqCanvasProxy *>(model());
        QWidget* editor = indexWidget(proxy->currentIndex());
        if (editor != nullptr)
        {
            closeEditor(editor, QAbstractItemDelegate::NoHint);
            edit(proxy->currentIndex());
        }
    }

    viewport()->update();
}


void CanvasView::paintEvent(QPaintEvent *event)
{
    if (!model())
        return;

    QPainter painter(viewport());
    // Factor in scroll offsets.
    painter.translate(-horizontalOffset(), -verticalOffset());

    QStyleOptionViewItem option;
    initViewItemOption(&option);
    option.rect = event->rect();


    FrameSeqCanvasProxy *proxy = static_cast<FrameSeqCanvasProxy *>(model());


    // Paint background.
    QPixmap zoomedBg = m_background.scaled(proxy->currentIndex().data().value<QPixmap>().size() *
                                           m_zoom);
    QPoint topLeft(std::max(0, option.rect.center().x() - zoomedBg.size().width() / 2),
                   std::max(0, option.rect.center().y() - zoomedBg.size().height() / 2));
    QRect centered(topLeft, zoomedBg.size());
    painter.drawPixmap(centered, zoomedBg);


    // Paint data.
    for (int row = 0; row < proxy->rowCount(); ++row)
    {
        // Opacity divided evenly among the frames within onion skinning range.
        float opacity = 1.f / (proxy->rowCount() - row);
        painter.setOpacity(opacity);

        QModelIndex index = proxy->index(row, 0);


        if (row == proxy->rowCount() - 1)
        {
            if (state() != QAbstractItemView::EditingState)
            {
                QPixmap current = index.data().value<QPixmap>();
                QPixmap zoomedCurrent = current.scaled(current.size() * m_zoom);

                painter.drawPixmap(centered, zoomedCurrent);
            }
            else
                // Paint the top-most item only for editors.
                itemDelegate()->paint(&painter, option, index);
        }
        else
        {
            QPixmap current = index.data().value<QPixmap>();
            QPixmap zoomedCurrent = current.scaled(current.size() * m_zoom);

            painter.drawPixmap(centered, zoomedCurrent);
        }
    }
}


void CanvasView::resizeEvent(QResizeEvent *) { updateGeometries(); }


void CanvasView::mousePressEvent(QMouseEvent *event)
{
    if (!model())
        return;

    if (state() != QAbstractItemView::EditingState)
    {
        FrameSeqCanvasProxy *proxy = static_cast<FrameSeqCanvasProxy *>(model());
        edit(proxy->currentIndex());
    }

    QAbstractItemView::mousePressEvent(event);
}


void CanvasView::mouseMoveEvent(QMouseEvent *)
{
    if (!model())
        return;

    if (state() != QAbstractItemView::EditingState)
    {
        FrameSeqCanvasProxy *proxy = static_cast<FrameSeqCanvasProxy *>(model());
        edit(proxy->currentIndex());
    }
}
