#ifndef ANIMPREVIEW_H
#define ANIMPREVIEW_H

#include <QAbstractItemView>


///
/// \brief Previews the items in the model 1 by 1.
///
/// See AnimPreviewWidget.
///
class AnimPreView : public QAbstractItemView
{
    Q_OBJECT
public:
    AnimPreView(QWidget *parent = nullptr);

public slots:
    void setZoom(float factor);
    void nextIndex();
    void prevIndex();
    void firstIndex();
    void lastIndex();
    void onTick();

private:
    float m_zoom;


    // QAbstractItemView interface
public:
    virtual QRect visualRect(const QModelIndex &) const override;
    virtual void scrollTo(const QModelIndex &, ScrollHint) override;
    virtual QModelIndex indexAt(const QPoint &) const override;
protected:
    virtual QModelIndex moveCursor(CursorAction, Qt::KeyboardModifiers) override;
    virtual int horizontalOffset() const override;
    virtual int verticalOffset() const override;
    virtual bool isIndexHidden(const QModelIndex &) const override;
    virtual void setSelection(const QRect &rect, QItemSelectionModel::SelectionFlags command) override;
    virtual QRegion visualRegionForSelection(const QItemSelection &selection) const override;
protected slots:
    virtual void dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight,
                             const QList<int> &roles) override;
    virtual void updateGeometries() override;

    // QWidget interface
protected:
    virtual void paintEvent(QPaintEvent *event) override;
    virtual void resizeEvent(QResizeEvent *) override;

    // QAbstractScrollArea interface
protected:
    virtual void scrollContentsBy(int dx, int dy) override;
};


#endif // ANIMPREVIEW_H
