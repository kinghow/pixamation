#include "canvas_view.h"

CanvasView::CanvasView(QWidget *parent)
    : QListView(parent) { setMouseTracking(true); }


void CanvasView::onZoomChanged()
{
    // Update item size when zoom changes and repaint.
//    emit itemDelegate()->sizeHintChanged(currentIndex());
    update();
}


// Trigger edit on mouse move.
void CanvasView::mouseMoveEvent(QMouseEvent *)
{
    // Call setCurrentIndex before edit from qt documentation.
    // Get first model is fine because canvas always show 1 item only.
    //setCurrentIndex(model()->index(0, 0));
//    edit(model()->index(0, 0));
}


void CanvasView::resizeEvent(QResizeEvent *event)
{
    QListView::resizeEvent(event);

    // Update item size when resized.
    emit itemDelegate()->sizeHintChanged(currentIndex());
}


// Prevent editor from closing.
void CanvasView::closeEditor(QWidget *, QAbstractItemDelegate::EndEditHint)
{
    return;
}


// Prevent auto scrolling.
void CanvasView::scrollTo(const QModelIndex &index, QAbstractItemView::ScrollHint hint)
{
    QListView::scrollTo(index, hint);
}
