#include "frame_seq_widget.h"

#include <QVBoxLayout>


FrameSeqWidget::FrameSeqWidget(QWidget *parent)
    : QWidget(parent)
    , m_frameNew(new QPushButton(QIcon(":/add.png"), "", this))
    , m_frameDelete(new QPushButton(QIcon(":/remove.png"), "", this))
{
    setSizePolicy(QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding));

    m_frameNew->setIconSize({ 16, 16 });
    m_frameNew->setSizePolicy(QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding));
    m_frameNew->setToolTip("Insert Frame");

    m_frameDelete->setIconSize({ 16, 16 });
    m_frameDelete->setSizePolicy(QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding));
    m_frameDelete->setToolTip("Remove Frame");


    connect(m_frameNew, &QPushButton::clicked, this, &FrameSeqWidget::onInsertButton);
    connect(m_frameDelete, &QPushButton::clicked, this, &FrameSeqWidget::onRemoveButton);


    QVBoxLayout *layout = new QVBoxLayout;
    layout->setSpacing(4);
    layout->setContentsMargins(0, 0, 0, 0);
    this->setLayout(layout);

    layout->addWidget(m_frameNew);
    layout->addWidget(m_frameDelete);
}


void FrameSeqWidget::onInsertButton() { emit insertFrames(); }


void FrameSeqWidget::onRemoveButton() { emit removeFrames(); }
