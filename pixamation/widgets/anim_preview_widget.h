#ifndef ANIMPREVIEWWIDGET_H
#define ANIMPREVIEWWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QSpinBox>
#include <QComboBox>
#include <QTimer>


///
/// \brief Widget for controlling the animation preview.
///
class AnimPreviewWidget : public QWidget
{
    Q_OBJECT
public:
    explicit AnimPreviewWidget(QWidget *parent = nullptr);
    int fps() const;

signals:
    void nextPressed();
    void prevPressed();
    void firstPressed();
    void lastPressed();
    void zoomChanged(float factor);
    void ticked();

private:
    std::vector<QPushButton *> m_buttons;
    QPushButton *m_playPauseButton;
    bool m_isPlaying;

    QLabel *m_fpsLabel;
    QSpinBox *m_fps;
    QLabel *m_zoomLabel;
    QComboBox *m_zoom;

    QTimer m_timer;
};


#endif // ANIMPREVIEWWIDGET_H
