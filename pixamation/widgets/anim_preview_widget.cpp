#include "anim_preview_widget.h"
#include <QGridLayout>


AnimPreviewWidget::AnimPreviewWidget(QWidget *parent)
    : QWidget(parent)
    , m_buttons()
    , m_playPauseButton()
    , m_isPlaying(false)
    , m_fpsLabel()
    , m_fps()
    , m_zoomLabel()
    , m_zoom()
    , m_timer(this)
{
    setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum));

    m_fpsLabel = new QLabel("Fps", this);
    m_fpsLabel->setToolTip("Frames/Second");
    m_fps = new QSpinBox(this);
    m_fps->setRange(1, 48);
    m_fps->setValue(6);

    QPushButton *next = new QPushButton(QIcon(":/right-arrow.png"), "", this);
    QPushButton *prev = new QPushButton(QIcon(":/left-arrow.png"), "", this);
    QPushButton *first = new QPushButton(QIcon(":/previous.png"), "", this);
    QPushButton *last = new QPushButton(QIcon(":/next.png"), "", this);
    m_playPauseButton = new QPushButton(QIcon(":/play-button.png"), "", this);

    next->setToolTip("Next Frame");
    prev->setToolTip("Previous Frame");
    first->setToolTip("Skip to First Frame");
    last->setToolTip("Skip to Last Frame");
    m_playPauseButton->setToolTip("Play/Pause");

    m_buttons.push_back(next);
    m_buttons.push_back(prev);
    m_buttons.push_back(first);
    m_buttons.push_back(last);
    m_buttons.push_back(m_playPauseButton);

    QSizePolicy sp(QSizePolicy::Minimum, QSizePolicy::Minimum);
    QSize iconSize(16, 16);
    QSize size(24, 24);
    for (auto it : m_buttons)
    {
        it->setSizePolicy(sp);
        it->setIconSize(iconSize);
        it->setFixedSize(size);
    }

    m_zoomLabel = new QLabel("Zoom");
    m_zoomLabel->setSizePolicy(QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum));
    m_zoom = new QComboBox(this);
    m_zoom->addItem("500%", QVariant(5.f));
    m_zoom->addItem("400%", QVariant(4.f));
    m_zoom->addItem("300%", QVariant(3.f));
    m_zoom->addItem("200%", QVariant(2.f));
    m_zoom->addItem("100%", QVariant(1.f));
    m_zoom->addItem("50%", QVariant(0.5f));
    m_zoom->addItem("25%", QVariant(0.25f));
    m_zoom->setCurrentIndex(4);
    m_zoom->setSizePolicy(QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum));

    m_timer.setTimerType(Qt::PreciseTimer);
    m_timer.setInterval(1.f / m_fps->value() * 1000.f);


    connect(m_fps, &QSpinBox::valueChanged, this,
            [this](int fps) { m_timer.setInterval(1.f / fps * 1000.f); });
    connect(next, &QPushButton::clicked, this, [this]() { emit nextPressed(); });
    connect(prev, &QPushButton::clicked, this, [this]() { emit prevPressed(); });
    connect(first, &QPushButton::clicked, this, [this]() { emit firstPressed(); });
    connect(last, &QPushButton::clicked, this, [this]() { emit lastPressed(); });
    connect(m_playPauseButton, &QPushButton::clicked, this, [this]()
    {
        if (!m_isPlaying)
        { m_playPauseButton->setIcon(QIcon(":/pause-button.png")); m_timer.start(); }
        else
        { m_playPauseButton->setIcon(QIcon(":/play-button.png")); m_timer.stop(); }
        m_isPlaying = !m_isPlaying;
    });
    connect(m_zoom, &QComboBox::currentIndexChanged, this, [this](int index)
    {
        m_zoom->setCurrentIndex(index);
        emit zoomChanged(m_zoom->itemData(m_zoom->currentIndex()).value<float>());
    });
    connect(&m_timer, &QTimer::timeout, this, [this]() { emit ticked(); });


    QHBoxLayout *layout = new QHBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);

    QHBoxLayout *left = new QHBoxLayout();
    left->setContentsMargins(0, 0, 0, 0);
    left->addWidget(m_fpsLabel);
    left->addWidget(m_fps);
    left->addStretch();
    layout->addLayout(left, 1);

    QHBoxLayout *center = new QHBoxLayout();
    center->setSpacing(2);
    center->setContentsMargins(0, 0, 0, 0);
    center->addWidget(first);
    center->addWidget(prev);
    center->addWidget(m_playPauseButton);
    center->addWidget(next);
    center->addWidget(last);
    layout->addLayout(center, 1);

    QHBoxLayout *right = new QHBoxLayout();
    right->setContentsMargins(0, 0, 0, 0);
    right->addStretch();
    right->addWidget(m_zoomLabel);
    right->addWidget(m_zoom);
    layout->addLayout(right, 1);

    this->setLayout(layout);
}


int AnimPreviewWidget::fps() const { return m_fps->value(); }
