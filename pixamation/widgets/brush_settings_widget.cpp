#include "brush_settings_widget.h"
#include <QHBoxLayout>


BrushSettingsWidget::BrushSettingsWidget(QWidget *parent)
    : QWidget(parent)
    , m_label(new QLabel("Brush Size"))
    , m_spinbox(new QSpinBox())
{
    setSizePolicy(QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum));

    QHBoxLayout *layout = new QHBoxLayout;
    this->setLayout(layout);

    m_spinbox->setRange(1, 50);
    m_spinbox->setValue(1);

    layout->addWidget(m_label);
    layout->addWidget(m_spinbox);

    connect(m_spinbox, &QSpinBox::valueChanged,
            this, [this](int size) { emit brushSizeChanged(size); });
}


// Emits sizeChanged.
void BrushSettingsWidget::setBrushSize(int size) { m_spinbox->setValue(size); }


int BrushSettingsWidget::brushSize() const { return m_spinbox->value(); }
