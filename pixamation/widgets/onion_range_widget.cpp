#include "onion_range_widget.h"
#include <QHBoxLayout>


OnionRangeWidget::OnionRangeWidget(QWidget *parent)
    : QWidget(parent)
    , m_label()
    , m_spinbox()
{
    setSizePolicy(QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum));

    m_label = new QLabel("Onion Skinning", this);
    m_spinbox = new QSpinBox(this);

    m_spinbox->setRange(0, 10);
    m_spinbox->setValue(0);


    QHBoxLayout *layout = new QHBoxLayout;
    this->setLayout(layout);

    layout->addWidget(m_label);
    layout->addWidget(m_spinbox);

    connect(m_spinbox, &QSpinBox::valueChanged,
            this, [this](int size) { setRange(size); });
}


void OnionRangeWidget::setRange(int range) { emit rangeChanged(range); }
