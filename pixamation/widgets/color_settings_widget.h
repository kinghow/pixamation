#ifndef COLORSETTINGSWIDGET_H
#define COLORSETTINGSWIDGET_H

#include <QWidget>
#include <QPushButton>


///
/// \brief Widget for selecting color.
///
class ColorSettingsWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ColorSettingsWidget(QWidget *parent = nullptr);
    QColor color() const;

signals:
    void colorChanged(const QColor &color);

public slots:
    void setColor(const QColor &color);

private slots:
    void startColorDialog();

private:
    QString buttonStyleWithColor(const QColor& color) const;

private:
    QPushButton *m_button;
    QColor m_color;
};


#endif // COLORSETTINGSWIDGET_H
