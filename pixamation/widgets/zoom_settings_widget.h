#ifndef ZOOMSETTINGSWIDGET_H
#define ZOOMSETTINGSWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QComboBox>


///
/// \brief Widget to control zoom factor.
///
class ZoomSettingsWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ZoomSettingsWidget(QWidget *parent = nullptr);
    float zoomFactor() const;
    void setZoom(float factor);

signals:
    void zoomChanged(float factor);

public slots:
    void setIndex(int index);

private:
    QLabel *m_label;
    QComboBox *m_combo;
};


#endif // ZOOMSETTINGSWIDGET_H
