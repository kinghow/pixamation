#ifndef BRUSHSETTINGSWIDGET_H
#define BRUSHSETTINGSWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QSpinBox>


///
/// \brief Widget for setting brush size.
///
class BrushSettingsWidget : public QWidget
{
    Q_OBJECT
public:
    explicit BrushSettingsWidget(QWidget *parent = nullptr);
    void setBrushSize(int size);
    int brushSize() const;

signals:
    void brushSizeChanged(int size);

private:
    QLabel *m_label;
    QSpinBox *m_spinbox;
};


#endif // BRUSHSETTINGSWIDGET_H
