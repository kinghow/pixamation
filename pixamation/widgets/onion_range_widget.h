#ifndef ONIONRANGEWIDGET_H
#define ONIONRANGEWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QSpinBox>


///
/// \brief Widget for controlling onion skinning range.
///
class OnionRangeWidget : public QWidget
{
    Q_OBJECT
public:
    explicit OnionRangeWidget(QWidget *parent = nullptr);
    void setRange(int range);

signals:
    void rangeChanged(int range);

private:
    QLabel *m_label;
    QSpinBox *m_spinbox;
};


#endif // ONIONRANGEWIDGET_H
