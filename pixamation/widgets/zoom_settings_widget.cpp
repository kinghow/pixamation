#include "zoom_settings_widget.h"
#include <QHBoxLayout>


ZoomSettingsWidget::ZoomSettingsWidget(QWidget *parent)
    : QWidget(parent)
    , m_label(new QLabel("Zoom"))
    , m_combo(new QComboBox())
{
    setSizePolicy(QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum));

    QHBoxLayout *layout = new QHBoxLayout;
    this->setLayout(layout);

    m_combo->addItem("500%", QVariant(5.f));
    m_combo->addItem("400%", QVariant(4.f));
    m_combo->addItem("300%", QVariant(3.f));
    m_combo->addItem("200%", QVariant(2.f));
    m_combo->addItem("100%", QVariant(1.f));
    m_combo->addItem("50%", QVariant(0.5f));
    m_combo->addItem("25%", QVariant(0.25f));

    m_combo->setCurrentIndex(3);

    layout->addWidget(m_label);
    layout->addWidget(m_combo);

    connect(m_combo, &QComboBox::currentIndexChanged, this, &ZoomSettingsWidget::setIndex);
}


float ZoomSettingsWidget::zoomFactor() const
{
    return m_combo->itemData(m_combo->currentIndex()).value<float>();
}


void ZoomSettingsWidget::setZoom(float factor)
{
    for (int i = 0; i < m_combo->count(); ++i)
        if (qFuzzyCompare(factor, m_combo->itemData(i).value<float>()))
        {
            m_combo->setCurrentIndex(i);
            emit zoomChanged(factor);
            return;
        }

    // Default to first item if choice not found.
    m_combo->setCurrentIndex(0);

    emit zoomChanged(factor);
}


void ZoomSettingsWidget::setIndex(int index)
{
    m_combo->setCurrentIndex(index);

    emit zoomChanged(zoomFactor());
}
