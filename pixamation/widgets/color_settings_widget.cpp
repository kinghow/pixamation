#include "color_settings_widget.h"
#include <QHBoxLayout>
#include <QColorDialog>


ColorSettingsWidget::ColorSettingsWidget(QWidget *parent)
    : QWidget(parent)
    , m_button(new QPushButton())
    , m_color(Qt::black)
{
    QHBoxLayout *layout = new QHBoxLayout;
    this->setLayout(layout);

    m_button->setStyleSheet(buttonStyleWithColor(Qt::black));
    m_button->update();

    layout->addWidget(m_button);

    connect(m_button, &QPushButton::clicked, this, &ColorSettingsWidget::startColorDialog);

    setToolTip("Color Selector");
}


QColor ColorSettingsWidget::color() const { return m_color; }


void ColorSettingsWidget::setColor(const QColor &color)
{
    m_color = color;
    emit colorChanged(m_color);

    m_button->setStyleSheet(buttonStyleWithColor(m_color));
    m_button->update();
}


void ColorSettingsWidget::startColorDialog()
{
    QColor color = QColorDialog::getColor(m_color);
    if (!color.isValid())
        return;

    setColor(color);
}


QString ColorSettingsWidget::buttonStyleWithColor(const QColor &color) const
{
    return "QPushButton {"
           "background-color: " + color.name() + ";"
           "border: 2px outset #696969;"
           "border-radius: 18px;"
           "min-width: 32px;"
           "min-height: 32px; }"
           ""
           "QPushButton:pressed {"
           "border: 2px inset #696969; }";
}
