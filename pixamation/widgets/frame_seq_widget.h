#ifndef FRAMESEQWIDGET_H
#define FRAMESEQWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QItemSelectionModel>


///
/// \brief Widget for inserting and removing frames in FrameSeqListView.
///
class FrameSeqWidget : public QWidget
{
    Q_OBJECT
public:
    explicit FrameSeqWidget(QWidget *parent = nullptr);

signals:
    void insertFrames();
    void removeFrames();

private slots:
    void onInsertButton();
    void onRemoveButton();

private:
    QPushButton *m_frameNew;
    QPushButton *m_frameDelete;
};


#endif // FRAMESEQWIDGET_H
