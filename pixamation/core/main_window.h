#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QMainWindow>
#include <QToolBar>
#include <QActionGroup>
#include <QDockWidget>
#include <QUndoView>

#include "models/frame_seq_model.h"
#include "models/frame_seq_canvas_proxy.h"
#include "views/frame_seq_list_view.h"
#include "views/canvas_view.h"
#include "views/anim_pre_view.h"
#include "delegates/frame_seq_list_delegate.h"
#include "delegates/base_canvas_delegate.h"
#include "delegates/brush_delegate.h"
#include "delegates/eye_dropper_delegate.h"
#include "widgets/anim_preview_widget.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void initMenus();
    void initViewsAndProxy();
    void initDelegates();
    void initCanvasToolBar();
    void initEditToolBar();
    void initAnimDock();
    void initAnimPreviewDock();
    void initCommandDock();

    void enableControls();
    void disableControls();

    void newDoc(const QSize &size, bool bypassDialog);

private slots:
    void newFrameSeq();
    void exportGif();
    void exportPngs();

private:
    QToolBar *m_canvasToolBar;
    QToolBar *m_editToolBar;

    FrameSeqModel *m_model;
    FrameSeqCanvasProxy *m_proxy;

    FrameSeqListView *m_framesView;
    CanvasView *m_canvasView;
    AnimPreView *m_previewView;

    FrameSeqListDelegate *m_frameListDel;
    BaseCanvasDelegate *m_canvasDel;
    BrushDelegate *m_brushDel;
    EyeDropperDelegate *m_dropperDel;

    QDockWidget *m_dockAnim;
    QDockWidget *m_dockAnimPreview;
    AnimPreviewWidget *m_animPreWidget;
    QDockWidget *m_dockCommand;
    QUndoView *m_undoView;

    QAction *m_showAnimWinAct;
    QAction *m_showPreviewWinAct;
    QAction *m_previewDockableAct;
    QAction *m_showCommandWinAct;
    QAction *m_undoAct;
    QAction *m_redoAct;


private:
    Ui::MainWindow *ui;
};

#endif // MAIN_WINDOW_H
