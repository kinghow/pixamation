#include "model_exporter.h"
#include "externals/gif.h"
#include <QPainter>


bool ModelExporter::frameSeqModelToImages(const FrameSeqModel *model, const QString &file)
{
    for (int row = 0; row < model->rowCount(); ++row)
    {
        QString suffixed(file);
        std::size_t dotIndex = suffixed.toStdString().find_last_of(".");
        suffixed.insert(dotIndex, "-" + QString::number(row));

        bool success = model->index(row).data().value<QPixmap>().save(suffixed);
        if (!success)
            return false;
    }

    return true;
}


void ModelExporter::frameSeqModelToGif(const FrameSeqModel *model, const QString &file, int fps)
{

    GifWriter *writer = new GifWriter;

    QSize frameSize = model->index(0).data().value<QPixmap>().size();
    int delay = 1.f / fps * 100.f;

    GifBegin(writer, qPrintable(file), frameSize.width(), frameSize.height(), delay);
    for (int row = 0; row < model->rowCount(); ++row)
    {
        // Library cannot handle transparent pixels. Paint the background white.
        QImage background(frameSize, QImage::Format_ARGB32);
        background.fill(QColor(Qt::white).rgb());
        QPainter painter(&background);
        painter.drawImage(0, 0, model->index(row).data().value<QPixmap>().toImage());

        // Gif writer does not support alpha.
        GifWriteFrame(writer, background.rgbSwapped().constBits() ,frameSize.width(),
                      frameSize.height(), delay, 8, false);
    }
    GifEnd(writer);
    // Writer freed.
}


ModelExporter::ModelExporter() {}
