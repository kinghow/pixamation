#ifndef MODELEXPORTER_H
#define MODELEXPORTER_H

#include "models/frame_seq_model.h"


///
/// \brief Utility class for exporting model.
///
class ModelExporter
{
public:
    static bool frameSeqModelToImages(const FrameSeqModel *model, const QString &file);
    static void frameSeqModelToGif(const FrameSeqModel *model, const QString &file, int fps);

private:
    ModelExporter();
};


#endif // MODELEXPORTER_H
