#include "main_window.h"
#include "ui_main_window.h"

#include <QLayout>
#include <QSizePolicy>
#include <QFileIconProvider>
#include <QMessageBox>
#include <QFileDialog>
#include "dialogs/new_doc_dialog.h"
#include "dialogs/credits_dialog.h"
#include "dialogs/features_dialog.h"
#include "commands/command_stack.h"
#include "widgets/frame_seq_widget.h"
#include "widgets/zoom_settings_widget.h"
#include "widgets/brush_settings_widget.h"
#include "widgets/color_settings_widget.h"
#include "widgets/onion_range_widget.h"
#include "core/model_exporter.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_canvasToolBar()
    , m_editToolBar()
    , m_model()
    , m_framesView()
    , m_undoView()
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QVBoxLayout *layout = new QVBoxLayout();
    ui->centralwidget->setLayout(layout);

    CommandStack::stack = new QUndoStack(this);
    CommandStack::stack->setUndoLimit(64);

    initMenus();

    initViewsAndProxy();
    initDelegates();
    initCanvasToolBar();
    initEditToolBar();
    initAnimDock();
    initAnimPreviewDock();
    initCommandDock();

    disableControls();
    newDoc({ 160, 160 }, true);
    enableControls();
}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::initMenus()
{
    QFileIconProvider provider;
    QIcon doc = provider.icon(QFileIconProvider::File);
//    QIcon dir = provider.icon(QFileIconProvider::Folder);

    QMenu *file = menuBar()->addMenu("File");

    file->addAction(doc, "New", Qt::CTRL | Qt::Key_N, this, &MainWindow::newFrameSeq);
    file->addAction("Export as separate images", this, &MainWindow::exportPngs);
    file->addAction("Export as .gif", this, &MainWindow::exportGif);
    file->addAction("Exit", this, &QWidget::close);

    QMenu *view = menuBar()->addMenu("View");

    QMenu *animMenu = view->addMenu("Animation Window");
    m_showAnimWinAct = new QAction("Show");
    m_showAnimWinAct->setCheckable(true);
    m_showAnimWinAct->setChecked(true);
    animMenu->addAction(m_showAnimWinAct);

    QMenu *animPreviewMenu = view->addMenu("Preview Window");
    m_showPreviewWinAct = new QAction("Show");
    m_showPreviewWinAct->setCheckable(true);
    m_showPreviewWinAct->setChecked(true);
    animPreviewMenu->addAction(m_showPreviewWinAct);
    m_previewDockableAct = new QAction("Snap to Dock");
    m_previewDockableAct->setCheckable(true);
    animPreviewMenu->addAction(m_previewDockableAct);

    QMenu *commandMenu = view->addMenu("History Window");
    m_showCommandWinAct = new QAction("Show");
    m_showCommandWinAct->setCheckable(true);
    m_showCommandWinAct->setChecked(false);
    commandMenu->addAction(m_showCommandWinAct);

    QMenu *about = menuBar()->addMenu("About");

    QAction *features = new QAction("Features");
    about->addAction(features);
    connect(features, &QAction::triggered, this,
            [this]() { FeaturesDialog prompt(this); prompt.exec(); });

    QAction *credits = new QAction("Credits");
    about->addAction(credits);
    connect(credits, &QAction::triggered, this,
            [this]() { CreditsDialog prompt(this); prompt.exec(); });
}


void MainWindow::initViewsAndProxy()
{
    m_framesView = new FrameSeqListView(this);
    m_canvasView = new CanvasView(this);
    m_previewView = new AnimPreView(this);

    m_proxy = new FrameSeqCanvasProxy(this);


    connect(m_framesView, &FrameSeqListView::currentIndexChanged,
            m_canvasView, &CanvasView::updateViewport);
    connect(m_framesView, &FrameSeqListView::currentIndexChanged,
            m_proxy, &FrameSeqCanvasProxy::setInvalidateRows);


    ui->centralwidget->layout()->addWidget(m_canvasView);
}


void MainWindow::initDelegates()
{
    assert(m_framesView != nullptr && "Frame list should be created before delegates.");
    assert(m_canvasView != nullptr && "Canvas should be created before delegates.");


    m_frameListDel = new FrameSeqListDelegate(m_framesView);
    m_canvasDel = new BaseCanvasDelegate(m_canvasView);
    m_brushDel = new BrushDelegate(m_canvasView);
    m_dropperDel = new EyeDropperDelegate(m_canvasView);

    m_framesView->setItemDelegate(m_frameListDel);
    m_canvasView->setItemDelegate(m_canvasDel);
}


void MainWindow::initCanvasToolBar()
{
    assert(m_canvasView != nullptr && "Canvas should be created before canvas toolbar.");
    assert(m_canvasDel != nullptr && "Delegates should be created before canvas toolbar.");
    assert(m_brushDel != nullptr && "Delegates should be created before canvas toolbar.");
    assert(m_proxy != nullptr && "Models should be created before canvas toolbar.");


    m_canvasToolBar = new QToolBar("Canvas Tools", this);
    m_canvasToolBar->setIconSize(QSize(32, 32));
    m_canvasToolBar->setOrientation(Qt::Horizontal);


    m_undoAct = new QAction(QIcon(":/undo.png"), "Undo (Ctrl + Z)");
    m_undoAct->setShortcut(QKeySequence::Undo);
    m_undoAct->setDisabled(true);

    m_redoAct = new QAction(QIcon(":/redo.png"), "Redo (Ctrl + Y)");
    m_redoAct->setShortcut(QKeySequence::Redo);
    m_redoAct->setDisabled(true);

    BrushSettingsWidget *brush = new BrushSettingsWidget;
    ZoomSettingsWidget *zoom = new ZoomSettingsWidget;
    OnionRangeWidget *onion = new OnionRangeWidget;


    m_canvasToolBar->addAction(m_undoAct);
    m_canvasToolBar->addAction(m_redoAct);
    m_canvasToolBar->addSeparator();
    m_canvasToolBar->addWidget(brush);
    m_canvasToolBar->addWidget(zoom);
    m_canvasToolBar->addWidget(onion);


    connect(m_undoAct, &QAction::triggered, this, [this]()
    {
        CommandStack::stack->undo();
        if (!CommandStack::stack->canUndo())
            m_undoAct->setDisabled(true);
    });
    connect(CommandStack::stack, &QUndoStack::canUndoChanged, this, [this](bool canUndo)
    {
        if (canUndo) m_undoAct->setDisabled(false);
        else m_undoAct->setDisabled(true);
    });
    connect(m_redoAct, &QAction::triggered, this, [this]()
    {
        CommandStack::stack->redo();
        if (!CommandStack::stack->canRedo())
            m_redoAct->setDisabled(true);
    });
    connect(CommandStack::stack, &QUndoStack::canRedoChanged, this, [this](bool canRedo)
    {
        if (canRedo) m_redoAct->setDisabled(false);
        else m_redoAct->setDisabled(true);
    });

    connect(brush, &BrushSettingsWidget::brushSizeChanged,
            m_canvasDel, &BaseCanvasDelegate::setBrushSize);
    connect(brush, &BrushSettingsWidget::brushSizeChanged,
            m_brushDel, &BaseCanvasDelegate::setBrushSize);
    connect(brush, &BrushSettingsWidget::brushSizeChanged,
            m_dropperDel, &BaseCanvasDelegate::setBrushSize);
    connect(zoom, &ZoomSettingsWidget::zoomChanged, m_canvasView, &CanvasView::setZoom);
    connect(zoom, &ZoomSettingsWidget::zoomChanged, m_canvasDel, &BrushDelegate::setZoom);
    connect(zoom, &ZoomSettingsWidget::zoomChanged, m_brushDel, &BrushDelegate::setZoom);
    connect(zoom, &ZoomSettingsWidget::zoomChanged, m_dropperDel, &BrushDelegate::setZoom);
    connect(onion, &OnionRangeWidget::rangeChanged,
            m_proxy, &FrameSeqCanvasProxy::setOnionRange);
    connect(m_proxy, &FrameSeqCanvasProxy::onionRangedChanged,
            m_canvasView, &CanvasView::updateViewport);


    brush->setBrushSize(10);
    zoom->setZoom(2.f);
    onion->setRange(0);


    addToolBar(Qt::TopToolBarArea, m_canvasToolBar);
}


void MainWindow::initEditToolBar()
{
    assert(m_canvasView != nullptr && "Canvas should be created before canvas toolbar.");
    assert(m_canvasDel != nullptr && "Delegates should be created before canvas toolbar.");
    assert(m_brushDel != nullptr && "Delegates should be created before canvas toolbar.");


    m_editToolBar = new QToolBar("Edit Tools", this);
    m_editToolBar->setIconSize(QSize(32, 32));
    m_editToolBar->setOrientation(Qt::Vertical);

    QActionGroup *editGroup = new QActionGroup(m_editToolBar);

    QAction *brush = new QAction(QIcon(":/paint-brush.png"), "Brush (B)", editGroup);
    brush->setShortcut(Qt::Key_B);
    brush->setCheckable(true);
    brush->setChecked(true);
    m_canvasView->setItemDelegate(m_brushDel);

    QAction *erase = new QAction(QIcon(":/eraser.png"), "Eraser (E)", editGroup);
    erase->setShortcut(Qt::Key_E);
    erase->setCheckable(true);

    QAction *dropper = new QAction(QIcon(":/dropper.png"), "Eye Dropper (I)", editGroup);
    dropper->setShortcut(Qt::Key_I);
    dropper->setCheckable(true);


    ColorSettingsWidget *color = new ColorSettingsWidget(m_editToolBar);

    m_editToolBar->addActions(editGroup->actions());
    m_editToolBar->addSeparator();
    m_editToolBar->addWidget(color);


    connect(brush, &QAction::triggered, this, [this]()
    {
        m_canvasView->setItemDelegate(m_canvasDel);
        m_canvasView->setItemDelegate(m_brushDel);
        m_brushDel->setEraseMode(false);
    });
    connect(erase, &QAction::triggered, this, [this]()
    {
        m_canvasView->setItemDelegate(m_canvasDel);
        m_canvasView->setItemDelegate(m_brushDel);
        m_brushDel->setEraseMode(true);
    });
    connect(dropper, &QAction::triggered, this, [this]()
    {
        m_canvasView->setItemDelegate(m_dropperDel);
    });
    connect(color, &ColorSettingsWidget::colorChanged,
            m_canvasDel, &BaseCanvasDelegate::setBrushColor);
    connect(color, &ColorSettingsWidget::colorChanged,
            m_brushDel, &BaseCanvasDelegate::setBrushColor);
    connect(m_dropperDel, &EyeDropperDelegate::colorPicked,
            color, &ColorSettingsWidget::setColor);


    color->setColor(color->color());


    addToolBar(Qt::LeftToolBarArea, m_editToolBar);
}


void MainWindow::initAnimDock()
{
    assert(m_framesView != nullptr && "Frame list should be created before animation dock.");


    m_dockAnim = new QDockWidget("Animation");
    QWidget *container = new QWidget(m_dockAnim);
    FrameSeqWidget *widget = new FrameSeqWidget(container);


    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(widget);
    layout->addWidget(m_framesView);
    container->setLayout(layout);

    m_dockAnim->setWidget(container);


    connect(widget, &FrameSeqWidget::insertFrames, m_framesView, &FrameSeqListView::insertRows);
    connect(widget, &FrameSeqWidget::removeFrames, m_framesView, &FrameSeqListView::removeRows);
    connect(m_showAnimWinAct, &QAction::triggered, this,
            [this](bool checkable) { m_dockAnim->setVisible(checkable); });
    connect(m_dockAnim, &QDockWidget::visibilityChanged, this,
            [this](bool visible) { m_showAnimWinAct->setChecked(visible); });


    addDockWidget(Qt::BottomDockWidgetArea, m_dockAnim);
}


void MainWindow::initAnimPreviewDock()
{
    assert(m_previewView != nullptr && "Preview view should be created before preview dock.");


    m_dockAnimPreview = new QDockWidget("Preview", this);
    // Disable snapping to dock by default.
    m_dockAnimPreview->setFeatures(QDockWidget::DockWidgetClosable |
                                   QDockWidget::DockWidgetFloatable);
    QWidget *container = new QWidget(m_dockAnimPreview);
    m_animPreWidget = new AnimPreviewWidget(container);


    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(m_previewView);
    layout->addWidget(m_animPreWidget);
    container->setLayout(layout);

    m_dockAnimPreview->setWidget(container);


    connect(m_animPreWidget, &AnimPreviewWidget::zoomChanged, m_previewView, &AnimPreView::setZoom);
    connect(m_animPreWidget, &AnimPreviewWidget::ticked, m_previewView, &AnimPreView::onTick);
    connect(m_animPreWidget, &AnimPreviewWidget::nextPressed, m_previewView, &AnimPreView::nextIndex);
    connect(m_animPreWidget, &AnimPreviewWidget::prevPressed, m_previewView, &AnimPreView::prevIndex);
    connect(m_animPreWidget, &AnimPreviewWidget::firstPressed, m_previewView, &AnimPreView::firstIndex);
    connect(m_animPreWidget, &AnimPreviewWidget::lastPressed, m_previewView, &AnimPreView::lastIndex);
    connect(m_showPreviewWinAct, &QAction::triggered, this,
            [this](bool checkable) { m_dockAnimPreview->setVisible(checkable); });
    connect(m_dockAnimPreview, &QDockWidget::visibilityChanged, this,
            [this](bool visible) { m_showPreviewWinAct->setChecked(visible); });
    connect(m_previewDockableAct, &QAction::triggered, this, [this](bool checkable)
    {
        auto features = m_dockAnimPreview->features();
        if (checkable)
            m_dockAnimPreview->setFeatures(features | QDockWidget::DockWidgetMovable);
        else
            m_dockAnimPreview->setFeatures(features & ~QDockWidget::DockWidgetMovable);
    });


    addDockWidget(Qt::BottomDockWidgetArea, m_dockAnimPreview);
    m_dockAnimPreview->setFloating(true);


    // Position at the top right corner of canvas.
    int left = m_dockAnimPreview->parentWidget()->pos().x() +
            m_dockAnimPreview->parentWidget()->size().width();
    int top = m_dockAnimPreview->parentWidget()->pos().y() + 140;
    left = left - m_dockAnimPreview->size().width() - 30;
    QPoint topLeft(left, top);
    QSize dockSize = m_dockAnimPreview->size();
    m_dockAnimPreview->setGeometry(QRect(topLeft, dockSize));
}


void MainWindow::initCommandDock()
{
    m_dockCommand = new QDockWidget("History");
    m_undoView = new QUndoView(CommandStack::stack);
    m_undoView->setEmptyLabel("<New>");

    m_dockCommand->setWidget(m_undoView);


    connect(m_showCommandWinAct, &QAction::triggered, this,
            [this](bool checkable) { m_dockCommand->setVisible(checkable); });
    connect(m_dockCommand, &QDockWidget::visibilityChanged, this,
            [this](bool visible) { m_showCommandWinAct->setChecked(visible); });


    addDockWidget(Qt::RightDockWidgetArea, m_dockCommand);

    // Hidden by default.
    m_dockCommand->hide();
}


void MainWindow::enableControls()
{
    m_dockAnim->widget()->setDisabled(false);
}


void MainWindow::disableControls()
{
    m_dockAnim->widget()->setDisabled(true);
}


void MainWindow::newDoc(const QSize& size, bool bypassDialog)
{
    if (!bypassDialog)
    {
        int result = QMessageBox::warning(this, "Warning!", "Unsaved changed will be lost. "
                                                            "Are you sure you want to proceed?",
                                          QMessageBox::Ok | QMessageBox::Cancel,
                                          QMessageBox::Cancel);

        if (result == QMessageBox::Cancel)
            return;
    }

    if (m_model != nullptr)
        delete m_model;

    m_model = new FrameSeqModel(size);

    m_framesView->setModel(m_model);
    m_framesView->selectionModel()->select(m_model->index(0), QItemSelectionModel::SelectCurrent);
    m_framesView->setCurrentIndex(m_model->index(0));

    m_proxy->setSourceModel(m_model);
    m_proxy->setRefSelectionModel(m_framesView->selectionModel());

    m_canvasView->setModel(m_proxy);
    m_canvasView->setCurrentIndex(m_proxy->index(0, 0));
    m_canvasView->setCanvasBackground(QPixmap(":/background-pattern.png"));

    m_previewView->setModel(m_model);
    m_previewView->setCurrentIndex(m_model->index(0));

    CommandStack::stack->clear();
}


void MainWindow::newFrameSeq()
{
    NewDocDialog prompt(this);

    if (prompt.exec() == NewDocDialog::Rejected)
        return;

    newDoc(prompt.inputSize(), false);
}


void MainWindow::exportPngs()
{
    QString filters("PNG files (*.png);;"
                    "JPEG files (*.jpg *.jpeg);;"
                    "BMP files (*.bmp);;"
                    "PPM files (*.ppm);;"
                    "X11 images (*.xbm *.xpm)");
    QString defaultFilter("PNG files (*.png)");
    auto filePath = QFileDialog::getSaveFileName(0, "Export as separate images", QDir::currentPath(),
                                                 filters, &defaultFilter);

    if (filePath.size() != 0)
    {
        if (ModelExporter::frameSeqModelToImages(m_model, filePath))
            return;
        else
        {
            QMessageBox *message = new QMessageBox(QMessageBox::Critical, "Error!",
                                                   "Export failed. File name not allowed or "
                                                   "unknown image format.");
            message->setAttribute(Qt::WA_DeleteOnClose, true);
            message->exec();
        }
    }
}


void MainWindow::exportGif()
{
    QString filters("GIF files (*.gif)");
    QString defaultFilter("GIF files (*.gif)");
    auto filePath = QFileDialog::getSaveFileName(0, "Export as .gif", QDir::currentPath(),
                                                 filters, &defaultFilter);

    if(filePath.right(3) == "gif")
    {
        ModelExporter::frameSeqModelToGif(m_model, filePath, m_animPreWidget->fps());
        return;
    }
    else if (filePath.size() != 0)
    {
        QMessageBox *message = new QMessageBox(QMessageBox::Critical, "Error!",
                                               "File extension must be .gif!");
        message->setAttribute(Qt::WA_DeleteOnClose, true);
        message->exec();
        exportGif();
    }
}
