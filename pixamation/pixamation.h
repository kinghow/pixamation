#ifndef PIXAMATION_H
#define PIXAMATION_H

class Pixamation
{
public:
    Pixamation();

    int run(int &argc, char **argv);
};

#endif // PIXAMATION_H
