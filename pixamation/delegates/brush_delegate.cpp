#include "brush_delegate.h"

#include <QMouseEvent>
#include "commands/command_stack.h"
#include "commands/brush_command.h"
#include "editors/brush_editor.h"
#include "models/frame_seq_canvas_proxy.h"


BrushDelegate::BrushDelegate(QObject *parent)
    : BaseCanvasDelegate(parent)
    , m_eraseMode(false) {}


void BrushDelegate::setEraseMode(bool erase) { m_eraseMode = erase; }


void BrushDelegate::setCommitData(BaseCanvasEditor *editor) { emit commitData(editor); }


QWidget *BrushDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                                     const QModelIndex &) const
{
    BrushEditor *editor = new BrushEditor(parent);
    editor->setZoom(m_zoom);
    editor->setBrushSize(m_brushSize);
    editor->setBrushColor(m_brushColor);
    editor->setPaintRect(option.rect);
    editor->setEraseMode(m_eraseMode);

    connect(editor, &BaseCanvasEditor::editingFinished, this, &BrushDelegate::setCommitData);
    connect(this, &BaseCanvasDelegate::zoomChanged, editor, &BaseCanvasEditor::setZoom);
    connect(this, &BaseCanvasDelegate::brushSizeChanged, editor, &BaseCanvasEditor::setBrushSize);
    connect(this, &BaseCanvasDelegate::brushColorChanged, editor, &BaseCanvasEditor::setBrushColor);

    return editor;
}


void BrushDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    BrushEditor *brush = static_cast<BrushEditor *>(editor);
    brush->setPaintCanvas(index.data().value<QPixmap>());
}


void BrushDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                 const QModelIndex &index) const
{
    BrushEditor *brush = static_cast<BrushEditor *>(editor);

    // Create undo action if paint canvas is dirty.
    if (brush->isDirty())
    {
        FrameSeqCanvasProxy *proxy = static_cast<FrameSeqCanvasProxy *>(model);
        FrameSeqModel *source = static_cast<FrameSeqModel *>(proxy->sourceModel());

        BrushCommand *cmd = new BrushCommand(source);
        cmd->setUndoState(proxy->mapToSource(index).row());

        model->setData(index, brush->paintCanvas());

        cmd->setRedoState(proxy->mapToSource(index).row());
        if (!m_eraseMode)
            cmd->setText("Paint on #" + QString::number(proxy->mapToSource(index).row()));
        else
            cmd->setText("Erase on #" + QString::number(proxy->mapToSource(index).row()));

        CommandStack::stack->push(cmd);
    }
    else
        model->setData(index, brush->paintCanvas());
}


void BrushDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option,
                                         const QModelIndex &) const
{
    QRect paintableRect = option.rect;
    paintableRect.translate(-option.rect.topLeft());

    editor->setGeometry(paintableRect);
}
