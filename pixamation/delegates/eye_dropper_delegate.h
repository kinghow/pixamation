#ifndef EYEDROPPERDELEGATE_H
#define EYEDROPPERDELEGATE_H

#include "delegates/base_canvas_delegate.h"
#include "editors/base_canvas_editor.h"


///
/// \brief Allows picking color on the canvas. Uses EyeDropperEditor to pick the color.
///
class EyeDropperDelegate : public BaseCanvasDelegate
{
    Q_OBJECT
public:
    EyeDropperDelegate(QObject *parent = nullptr);

signals:
    void colorPicked(const QColor &color);

private slots:
    void setColorData(BaseCanvasEditor *editor);


    // QAbstractItemDelegate interface
public:
    virtual QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const override;
    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    virtual void setModelData(QWidget *editor, QAbstractItemModel *model,
                              const QModelIndex &index) const override;
    virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option,
                                      const QModelIndex &) const override;

};


#endif // EYEDROPPERDELEGATE_H
