#ifndef BASECANVASDELEGATE_H
#define BASECANVASDELEGATE_H

#include <QStyledItemDelegate>


///
/// \brief A base class for delegates that require canvas information such as zoom. This delegate
/// does nothing on edit mode.
///
class BaseCanvasDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    BaseCanvasDelegate(QObject *parent = nullptr);

signals:
    void zoomChanged(float factor);
    void brushSizeChanged(int size);
    void brushColorChanged(const QColor &color);

public slots:
    void setZoom(float factor);
    void setBrushSize(int size);
    void setBrushColor(const QColor &color);

protected:
    float m_zoom;
    float m_brushSize;
    QColor m_brushColor;


    // QAbstractItemDelegate interface
public:
    virtual QSize sizeHint(const QStyleOptionViewItem &option,
                           const QModelIndex &index) const override;
    virtual void setEditorData(QWidget *, const QModelIndex &) const override;
    virtual void setModelData(QWidget *, QAbstractItemModel *, const QModelIndex &) const override;
};


#endif // BASECANVASDELEGATE_H
