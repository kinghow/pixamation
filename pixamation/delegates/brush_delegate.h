#ifndef BRUSHDELEGATE_H
#define BRUSHDELEGATE_H

#include "delegates/base_canvas_delegate.h"
#include "editors/base_canvas_editor.h"


///
/// \brief For painting and erasing on the canvas. Uses BrushEditor to edit.
///
/// See BrushEditor.
///
class BrushDelegate : public BaseCanvasDelegate
{
    Q_OBJECT
public:
    BrushDelegate(QObject *parent = nullptr);
    void setEraseMode(bool erase);

private slots:
    void setCommitData(BaseCanvasEditor *editor);

private:
    bool m_eraseMode;


    // QAbstractItemDelegate interface
public:
    virtual QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                                  const QModelIndex &) const override;
    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    virtual void setModelData(QWidget *editor, QAbstractItemModel *model,
                              const QModelIndex &index) const override;
    virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option,
                                      const QModelIndex &) const override;
};


#endif // BRUSHDELEGATE_H
