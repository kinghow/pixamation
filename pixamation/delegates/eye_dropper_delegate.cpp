#include "eye_dropper_delegate.h"

#include <QEvent>
#include <QMouseEvent>
#include "editors/eye_dropper_editor.h"


EyeDropperDelegate::EyeDropperDelegate(QObject *parent)
    : BaseCanvasDelegate(parent) {}


void EyeDropperDelegate::setColorData(BaseCanvasEditor *editor)
{
    EyeDropperEditor *dropper = static_cast<EyeDropperEditor *>(editor);
    emit colorPicked(dropper->colorPicked());
}


QWidget *EyeDropperDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                                          const QModelIndex &index) const
{
    EyeDropperEditor *editor = new EyeDropperEditor(parent);
    editor->setZoom(m_zoom);
    editor->setBrushSize(m_brushSize);
    editor->setBrushColor(m_brushColor);
    editor->setPaintRect(option.rect);
    editor->setPaintCanvas(index.data().value<QPixmap>());

    editor->setModelIndex(index);

    connect(editor, &BaseCanvasEditor::editingFinished, this, &EyeDropperDelegate::setColorData);
    connect(this, &BaseCanvasDelegate::zoomChanged, editor, &BaseCanvasEditor::setZoom);
    connect(this, &BaseCanvasDelegate::brushSizeChanged, editor, &BaseCanvasEditor::setBrushSize);

    return editor;
}


void EyeDropperDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    EyeDropperEditor *dropper = static_cast<EyeDropperEditor *>(editor);
    dropper->setPaintCanvas(index.data().value<QPixmap>());
}


void EyeDropperDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                      const QModelIndex &index) const
{
    EyeDropperEditor *dropper = static_cast<EyeDropperEditor *>(editor);
    model->setData(index, dropper->paintCanvas());
}


void EyeDropperDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option,
                                              const QModelIndex &) const
{
    QRect paintableRect = option.rect;
    paintableRect.translate(-option.rect.topLeft());

    editor->setGeometry(paintableRect);
}
