#ifndef FRAMESEQLISTDELEGATE_H
#define FRAMESEQLISTDELEGATE_H

#include <QStyledItemDelegate>


///
/// \brief Paints the items at the center with X_GAP horizontal margins.
///
class FrameSeqListDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    FrameSeqListDelegate(QObject *parent = nullptr);

private:
    // Horizontal gap between the items.
    static const int X_GAP;

    // Maximum size for an item.
    static const QSizeF MAX_ITEM_SIZE;


    // QAbstractItemDelegate interface
public:
    virtual void paint(QPainter *painter, const QStyleOptionViewItem &option,
                     const QModelIndex &index) const override;
    virtual QSize sizeHint(const QStyleOptionViewItem &,
                           const QModelIndex &index) const override;
};


#endif // FRAMESEQLISTDELEGATE_H
