#include "frame_seq_list_delegate.h"

#include <QPainter>


// left margin = X_GAP / 2, right margin = X_GAP / 2.
const int FrameSeqListDelegate::X_GAP = 4;

const QSizeF FrameSeqListDelegate::MAX_ITEM_SIZE = QSizeF(80.f, 80.f);


FrameSeqListDelegate::FrameSeqListDelegate(QObject *parent)
    : QStyledItemDelegate(parent) {}


void FrameSeqListDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
                             const QModelIndex &index) const
{
    QStyledItemDelegate::paint(painter, option, index);

    painter->save();

    int top = (option.rect.height() - option.rect.top()) / 2 -
              ((sizeHint(option, index) - QSize(X_GAP * 2, X_GAP * 2)).height() / 2);
    QRect centered = option.rect.translated(X_GAP, top);
    centered.setSize(sizeHint(option, index) - QSize(X_GAP * 2, X_GAP * 2));

    QPixmap scaled = index.data().value<QPixmap>().
            scaled(MAX_ITEM_SIZE.toSize(), Qt::KeepAspectRatio);

    painter->drawPixmap(centered, scaled);

    // Draw border around the item.
    QPen pen(Qt::black, 1);
    painter->setPen(pen);
    painter->drawRect(centered.adjusted(-2, -2, 2, 2));

    painter->restore();
}


QSize FrameSeqListDelegate::sizeHint(const QStyleOptionViewItem &, const QModelIndex &index) const
{
    QSize reduced = index.data().value<QPixmap>().size();
    float factor = 0.f;
    if (reduced.width() > reduced.height())
        factor = MAX_ITEM_SIZE.width() / reduced.width();
    else
        factor = MAX_ITEM_SIZE.height() / reduced.height();

    reduced.setWidth(reduced.width() * factor);
    reduced.setHeight(reduced.height() * factor);

    return reduced + QSize(X_GAP * 2, X_GAP * 2);
}
