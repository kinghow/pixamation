#include "base_canvas_delegate.h"
#include <QPainter>


BaseCanvasDelegate::BaseCanvasDelegate(QObject *parent)
    : QStyledItemDelegate(parent)
    , m_zoom(1.f)
    , m_brushSize(1) {}


void BaseCanvasDelegate::setZoom(float factor)
{
    m_zoom = factor;
    emit zoomChanged(factor);
}


void BaseCanvasDelegate::setBrushSize(int size)
{
    m_brushSize = size;
    emit brushSizeChanged(size);
}


void BaseCanvasDelegate::setBrushColor(const QColor &color)
{
    m_brushColor = color;
    emit brushColorChanged(color);
}


QSize BaseCanvasDelegate::sizeHint(const QStyleOptionViewItem &option,
                                   const QModelIndex &index) const
{
    QSizeF size = index.data().value<QPixmap>().size();

    QSize iconSize(static_cast<int>(size.width() * m_zoom),
               std::max(static_cast<int>(size.height() * m_zoom),
                        option.widget->size().height()));

    return size.toSize() * m_zoom;
}


// Do nothing on edit.
void BaseCanvasDelegate::setEditorData(QWidget *, const QModelIndex &) const {}


// Do nothing on finished editing.
void BaseCanvasDelegate::setModelData(QWidget *, QAbstractItemModel *, const QModelIndex &) const {}
