#include "pixamation.h"
#include "core/main_window.h"
#include <QApplication>

Pixamation::Pixamation()
{
}

int Pixamation::run(int &argc, char **argv)
{
    QApplication app(argc, argv);
    MainWindow window;
    window.show();
    return app.exec();
}
