#include "move_frame_command.h"

MoveFrameCommand::MoveFrameCommand(FrameSeqModel *model, QItemSelectionModel *selectionModel,
                                   int beginRow, QUndoCommand *parent)
    : QUndoCommand(parent)
    , m_model(model)
    , m_selectionModel(selectionModel)
    , m_undoAffectedRows()
    , m_redoAffectedRows()
    , m_undoSourceRows()
    , m_beginRow(beginRow)
{
    setText("Move [");

    for (const auto &index : selectionModel->selectedIndexes())
    {
        setText(text().append(QString::number(index.row())));
        setText(text().append(", "));

        // Remember initial states.
//        m_movingIndices.append(index);
        m_undoAffectedRows.push_back(index.row());
        m_undoSourceRows.push_back(index.row());
    }

    // Sort in ascending order so that items will remain in the same visual order.
    std::sort(m_undoAffectedRows.begin(), m_undoAffectedRows.end(), std::less<int>());

    setText(text().chopped(2));
    setText(text().append("] to [" + QString::number(beginRow) + "]"));
}


void MoveFrameCommand::undo()
{
    // QPersistentModelIndex makes it so that the indices are changed but still valid after moving.
    QList<QPersistentModelIndex> movingIndices;
    for (int row : m_redoAffectedRows)
        movingIndices.append(m_model->index(row));


    // Move items back in the reverse order.
    for (int i = movingIndices.size() - 1; i >= 0; --i)
    {
        int source = movingIndices.at(i).row();
        int dest = m_undoSourceRows.at(movingIndices.size() - i - 1);

        if (dest > source)
            m_model->moveRow(QModelIndex(), source, QModelIndex(), dest + 1);
        else
            m_model->moveRow(QModelIndex(), source, QModelIndex(), dest);
    }
}


void MoveFrameCommand::redo()
{
    // QPersistentModelIndex makes it so that the indices are changed but still valid after moving.
    QList<QPersistentModelIndex> movingIndices;
    for (int row : m_undoAffectedRows)
        movingIndices.append(m_model->index(row));


    int beginRow = m_beginRow;
    bool setRedoAffectedRows = m_redoAffectedRows.size() == 0;
    bool setRowsToIndices = m_undoSourceRows.size() == 0;
    for (auto it = movingIndices.begin(); it != movingIndices.end(); ++it)
    {
        // Remember the original rows the first time.
        if (setRowsToIndices)
            m_undoSourceRows.push_back(it->row());

        // Remember the position to where it was moved.
        if (setRedoAffectedRows)
            m_redoAffectedRows.push_back(beginRow - movingIndices.indexOf(*it) -  1);

        if (beginRow != it->row())
            // Only move when not in same position.
            m_model->moveRow(QModelIndex(), it->row(), QModelIndex(), beginRow);

        // Move the next item contiguously to the right.
        beginRow = it->row() + 1;
    }
}
