#include "brush_command.h"


BrushCommand::BrushCommand(FrameSeqModel *model, QUndoCommand *parent)
    : QUndoCommand(parent)
    , m_model(model)
    , m_row()
    , m_oldState()
    , m_newState() {}


void BrushCommand::setUndoState(int row)
{
    QModelIndex index = m_model->index(row);

    assert(index.isValid());
    m_oldState = index.data().value<QPixmap>();
    m_row = row;
}


void BrushCommand::setRedoState(int row)
{
    QModelIndex index = m_model->index(row);

    assert(index.isValid());
    m_newState = index.data().value<QPixmap>();
}


void BrushCommand::undo()
{
    assert(m_model);
    QModelIndex index = m_model->index(m_row);

    assert(index.isValid());
    m_model->setData(index, m_oldState, Qt::EditRole);
}


void BrushCommand::redo()
{
    assert(m_model);
    QModelIndex index = m_model->index(m_row);

    assert(index.isValid());
    m_model->setData(index, m_newState, Qt::EditRole);
}
