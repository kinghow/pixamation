#ifndef MOVEFRAMECOMMAND_H
#define MOVEFRAMECOMMAND_H

#include <QUndoCommand>
#include <QItemSelectionModel>
#include "models/frame_seq_model.h"


///
/// \brief Represents a frame move (rearrangement) action.
///
class MoveFrameCommand : public QUndoCommand
{
public:
    MoveFrameCommand(FrameSeqModel *model, QItemSelectionModel *selectionModel, int beginRow,
                     QUndoCommand *parent = nullptr);

private:
    FrameSeqModel *m_model;
    QItemSelectionModel *m_selectionModel;

    std::vector<int> m_undoAffectedRows;
    std::vector<int> m_redoAffectedRows;
    std::vector<int> m_undoSourceRows;
    int m_beginRow;


    // QUndoCommand interface
public:
    virtual void undo() override;
    virtual void redo() override;
};

#endif // MOVEFRAMECOMMAND_H
