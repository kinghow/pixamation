#ifndef COMMANDSTACK_H
#define COMMANDSTACK_H

#include <QUndoStack>


///
/// \brief Global reference for the command stack allows the command stack to be accessed by any
/// module to push commands onto. However, the limitation is that there can only be 1 stack.
///
class CommandStack
{
public:
    static QUndoStack *stack;
};


#endif // COMMANDSTACK_H
