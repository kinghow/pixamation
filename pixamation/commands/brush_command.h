#ifndef BRUSHCOMMAND_H
#define BRUSHCOMMAND_H

#include <QUndoCommand>
#include <QPixmap>
#include "models/frame_seq_model.h"


///
/// \brief Represents a paint/erase action. Use setUndoState to set the state of the canvas before
/// the command and setRedoState to set the state of the canvas after the command.
///
class BrushCommand : public QUndoCommand
{
public:
    BrushCommand(FrameSeqModel *model, QUndoCommand *parent = nullptr);
    void setUndoState(int row);
    void setRedoState(int row);

private:
    FrameSeqModel *m_model;
    int m_row;
    QPixmap m_oldState;
    QPixmap m_newState;


    // QUndoCommand interface
public:
    virtual void undo() override;
    virtual void redo() override;
};


#endif // BRUSHCOMMAND_H
