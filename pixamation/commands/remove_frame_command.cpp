#include "remove_frame_command.h"

RemoveFrameCommand::RemoveFrameCommand(FrameSeqModel *model, QItemSelectionModel *selectionModel,
                                       QUndoCommand *parent)
    : QUndoCommand(parent)
    , m_model(model)
    , m_selectionModel(selectionModel)
    , m_undoFramesState()
    , m_undoCurrentState()
    , m_undoSelectionState()
    , m_redoSelectionState()
{
    // Remember initial states.
    for (const auto &index : selectionModel->selectedIndexes())
        m_undoSelectionState.push_back(index.row());
    m_undoCurrentState = selectionModel->currentIndex().row();

    setText("Remove at [");
}


void RemoveFrameCommand::undo()
{
    // Need to insert in ascending order due to invalid rows from the end.
    // m_redoSelectionState is already sorted in descending order from redo.
    for (auto it = m_redoSelectionState.crbegin(); it != m_redoSelectionState.crend(); ++it)
    {
        int stride = *it;

        // Insert after the current item.
        m_model->insertRow(stride);

        // Restore pixmap.
        QModelIndex index = m_model->index(stride);
        m_model->setData(index, m_undoFramesState.at(m_redoSelectionState.crend() - it - 1),
                         Qt::EditRole);
    }

    // Restore selection.
    m_selectionModel->clearSelection();
    for (int row : m_undoSelectionState)
        m_selectionModel->select(m_model->index(row, 0), QItemSelectionModel::Select);

    // Restore current.
    QModelIndex next = m_model->index(m_undoCurrentState, 0);
    m_selectionModel->setCurrentIndex(next, QItemSelectionModel::NoUpdate);
}


void RemoveFrameCommand::redo()
{
    // Initialize affected rows.
    std::vector<int> rows;
    if (m_undoSelectionState.size() > 0)
        for (auto &row : m_undoSelectionState)
            rows.push_back(row);
    else
        rows.push_back(m_undoCurrentState);


    // If everything is selected, remove the last element selected from the affected rows.
    if (static_cast<int>(m_undoSelectionState.size()) == m_model->rowCount())
    {
        auto finder = std::find(rows.begin(), rows.end(), m_undoCurrentState);
        rows.erase(finder);
    }


    // Set command text the first time.
    if (m_redoSelectionState.size() == 0)
        for (std::size_t i = 0; i < rows.size(); ++i)
        {
            QString str = QString::number(rows[i]);
            if (i != rows.size() - 1)
                str.append(", ");
            else
                str.append("]");

            setText(text().append(str));
        }


    // Sort the rows because selection indices aren't necessarily in order.
    std::sort(rows.begin(), rows.end(), std::greater<int>());


    // Remember the frames state the first time.
    if (m_undoFramesState.size() == 0)
        for (auto row : rows)
        {
            QModelIndex index = m_model->index(row);
            // Index should be valid because every row was picked from selection.
            assert(index.isValid());
            m_undoFramesState.push_back(m_model->data(index, Qt::DisplayRole).value<QPixmap>());
        }


    // Remove in descending order so that earlier elements aren't affected.
    int itemsBelowCurrent = 0;
    for (auto row : rows)
    {
        // Keep track of the number of items below the current item to move the current index to
        // the correct position later.
        if (row < m_undoCurrentState)
            ++itemsBelowCurrent;

        m_model->removeRow(row);
    }

    // Always try to set the current item to the closest position from the last item removed.
    QModelIndex next = m_model->index(m_undoCurrentState - itemsBelowCurrent, 0);
    if (!next.isValid())
        next = m_model->index(m_undoCurrentState - itemsBelowCurrent - 1, 0);

    m_selectionModel->select(next, QItemSelectionModel::Select);
    m_selectionModel->setCurrentIndex(next, QItemSelectionModel::NoUpdate);

    // Remember removed elements the first time.
    if (m_redoSelectionState.size() == 0)
        std::copy(rows.begin(), rows.end(), std::back_inserter(m_redoSelectionState));
}
