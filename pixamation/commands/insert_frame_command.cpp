#include "insert_frame_command.h"

InsertFrameCommand::InsertFrameCommand(FrameSeqModel *model, QItemSelectionModel *selectionModel,
                                       QUndoCommand *parent)
    : QUndoCommand(parent)
    , m_model(model)
    , m_selectionModel(selectionModel)
    , m_redoFramesState()
    , m_undoCurrentState()
    , m_undoSelectionState()
    , m_redoSelectionState()
{
    // Remember initial states.
    for (const auto &index : selectionModel->selectedIndexes())
        m_undoSelectionState.push_back(index.row());
    m_undoCurrentState = selectionModel->currentIndex().row();

    setText("Insert at [");
}


void InsertFrameCommand::undo()
{
    // m_redoSelectionState was already sorted in ascending order from redo.
    // Remove in descending order so that earlier elements aren't affected.
    for (auto it = m_redoSelectionState.rbegin(); it != m_redoSelectionState.rend(); ++it)
        m_model->removeRow(*it);

    // Restore selection.
    m_selectionModel->clearSelection();
    for (int row : m_undoSelectionState)
        m_selectionModel->select(m_model->index(row, 0), QItemSelectionModel::Select);

    // Restore current.
    QModelIndex next = m_model->index(m_undoCurrentState, 0);
    m_selectionModel->setCurrentIndex(next, QItemSelectionModel::NoUpdate);
}


void InsertFrameCommand::redo()
{
    // Initialize affected rows.
    std::vector<int> rows;
    if (m_undoSelectionState.size() > 0)
        for (auto &row : m_undoSelectionState)
            rows.push_back(row);
    else
        rows.push_back(m_undoCurrentState);

    // Set command text the first time.
    if (m_redoSelectionState.size() == 0)
        for (std::size_t i = 0; i < rows.size(); ++i)
        {
            QString str = QString::number(rows[i]);
            if (i != rows.size() - 1)
                str.append(", ");
            else
                str.append("]");

            setText(text().append(str));
        }


    // Sort in descending order so that operations do not affect earlier elements.
    // Sort the rows because selection indices aren't necessarily in order.
    std::sort(rows.begin(), rows.end(), std::greater<int>());
    for (auto row : rows)
        // Insert after the current item.
        m_model->insertRow(row + 1);


    // Update selection and current.
    m_selectionModel->clearSelection();
    bool setSelectionState = m_redoSelectionState.size() == 0;
    bool setFramesState = m_redoFramesState.size() == 0;
    int itemsAboveCurrent = 0;
    for (auto it = rows.rbegin(); it != rows.rend(); ++it)
    {
        // Keep track of the number of items above the current item to move the current index to
        // the correct position later.
        if (*it > m_undoCurrentState)
            ++itemsAboveCurrent;

        // Account for every item inserted.
        int stride = *it + it - rows.crbegin() + 1;
        QModelIndex index = m_model->index(stride, 0);
        m_selectionModel->select(index, QItemSelectionModel::Select);

        // Remember selection state the first time.
        if (setSelectionState)
            m_redoSelectionState.push_back(stride);

        // Remember frames state the first time. Otherwise, restore the frames state.
        if (setFramesState)
            m_redoFramesState.push_back(index.data().value<QPixmap>());
        else
            m_model->setData(index, m_redoFramesState.at(it - rows.crbegin()), Qt::EditRole);
    }


    // Set current.
    QModelIndex next = m_model->index(m_undoCurrentState + rows.size() - itemsAboveCurrent, 0);
    m_selectionModel->setCurrentIndex(next, QItemSelectionModel::NoUpdate);
}
