#ifndef INSERTFRAMECOMMAND_H
#define INSERTFRAMECOMMAND_H

#include <QUndoCommand>
#include <QItemSelectionModel>
#include <QPixmap>
#include "models/frame_seq_model.h"


///
/// \brief Represents a frame insert action.
///
class InsertFrameCommand : public QUndoCommand
{
public:
    InsertFrameCommand(FrameSeqModel *model, QItemSelectionModel *selectionModel,
                       QUndoCommand *parent = nullptr);

private:
    FrameSeqModel *m_model;
    QItemSelectionModel *m_selectionModel;

    std::vector<QPixmap> m_redoFramesState;
    int m_undoCurrentState;
    std::vector<int> m_undoSelectionState;
    std::vector<int> m_redoSelectionState;


    // QUndoCommand interface
public:
    virtual void undo() override;
    virtual void redo() override;
};

#endif // INSERTFRAMECOMMAND_H
