QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = lib

DESTDIR = ../bin

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

PRECOMPILED_HEADER = pch.h

SOURCES += \
    commands/brush_command.cpp \
    commands/command_stack.cpp \
    commands/insert_frame_command.cpp \
    commands/move_frame_command.cpp \
    commands/remove_frame_command.cpp \
    core/model_exporter.cpp \
    core/main_window.cpp \
    delegates/base_canvas_delegate.cpp \
    delegates/brush_delegate.cpp \
    delegates/eye_dropper_delegate.cpp \
    delegates/frame_seq_list_delegate.cpp \
    dialogs/credits_dialog.cpp \
    dialogs/features_dialog.cpp \
    dialogs/new_doc_dialog.cpp \
    editors/base_canvas_editor.cpp \
    editors/brush_editor.cpp \
    editors/eye_dropper_editor.cpp \
    models/frame_seq_canvas_proxy.cpp \
    models/frame_seq_model.cpp \
    pixamation.cpp \
    views/anim_pre_view.cpp \
    views/canvas_view.cpp \
    views/frame_seq_list_view.cpp \
    widgets/anim_preview_widget.cpp \
    widgets/brush_settings_widget.cpp \
    widgets/color_settings_widget.cpp \
    widgets/frame_seq_widget.cpp \
    widgets/onion_range_widget.cpp \
    widgets/zoom_settings_widget.cpp

HEADERS += \
    commands/brush_command.h \
    commands/command_stack.h \
    commands/insert_frame_command.h \
    commands/move_frame_command.h \
    commands/remove_frame_command.h \
    core/model_exporter.h \
    core/main_window.h \
    delegates/base_canvas_delegate.h \
    delegates/brush_delegate.h \
    delegates/eye_dropper_delegate.h \
    delegates/frame_seq_list_delegate.h \
    dialogs/credits_dialog.h \
    dialogs/features_dialog.h \
    dialogs/new_doc_dialog.h \
    editors/base_canvas_editor.h \
    editors/brush_editor.h \
    editors/eye_dropper_editor.h \
    externals/gif.h \
    models/frame_seq_canvas_proxy.h \
    models/frame_seq_model.h \
    pch.h \
    pixamation.h \
    views/anim_pre_view.h \
    views/canvas_view.h \
    views/frame_seq_list_view.h \
    widgets/anim_preview_widget.h \
    widgets/brush_settings_widget.h \
    widgets/color_settings_widget.h \
    widgets/frame_seq_widget.h \
    widgets/onion_range_widget.h \
    widgets/zoom_settings_widget.h

FORMS += \
    core/main_window.ui \
    dialogs/credits_dialog.ui \
    dialogs/features_dialog.ui \
    dialogs/new_doc_dialog.ui

# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    ../images/images.qrc
