#ifndef NEW_DOC_DIALOG_H
#define NEW_DOC_DIALOG_H

#include <QDialog>


namespace Ui {
class NewDocDialog;
}

///
/// \brief Prompt for dimension inputs.
///
class NewDocDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewDocDialog(QWidget *parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
    ~NewDocDialog();
    QSize inputSize() const;


private:
    Ui::NewDocDialog *ui;

    // QDialog interface
public slots:
    virtual void accept() override;
};


#endif // NEW_DOC_DIALOG_H
