#ifndef CREDITS_DIALOG_H
#define CREDITS_DIALOG_H

#include <QDialog>


namespace Ui {
class CreditsDialog;
}

///
/// \brief Prompt for credits information.
///
class CreditsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreditsDialog(QWidget *parent = nullptr);
    ~CreditsDialog();

private:
    Ui::CreditsDialog *ui;
};


#endif // CREDITS_DIALOG_H
