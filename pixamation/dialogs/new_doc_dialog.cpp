#include "new_doc_dialog.h"
#include "ui_new_doc_dialog.h"
#include <QIntValidator>
#include <QMessageBox>


NewDocDialog::NewDocDialog(QWidget *parent, Qt::WindowFlags f)
    : QDialog(parent, f)
    , ui(new Ui::NewDocDialog)
{
    ui->setupUi(this);

    ui->lineEditWidth->setValidator(new QIntValidator(1, 1920, this));
    ui->lineEditHeight->setValidator(new QIntValidator(1, 1080, this));
}


NewDocDialog::~NewDocDialog()
{
    delete ui;
}


QSize NewDocDialog::inputSize() const
{
    return { ui->lineEditWidth->text().toInt(), ui->lineEditHeight->text().toInt() };
}


void NewDocDialog::accept()
{
    QString str;
    int pos = 0;

    str = ui->lineEditWidth->text();
    if (ui->lineEditWidth->validator()->validate(str, pos) != QValidator::Acceptable)
    {
        QMessageBox *message = new QMessageBox(QMessageBox::Critical, "Error!",
                                               "Pixamation only supports up to 1920x1080.");
        message->setAttribute(Qt::WA_DeleteOnClose, true);
        message->exec();
        return;
    }

    str = ui->lineEditHeight->text();
    if (ui->lineEditHeight->validator()->validate(str, pos) != QValidator::Acceptable)
    {
        QMessageBox *message = new QMessageBox(QMessageBox::Critical, "Error!",
                                               "Pixamation only supports up to 1920x1080.");
        message->setAttribute(Qt::WA_DeleteOnClose, true);
        message->exec();
        return;
    }

    QDialog::accept();
}
