#ifndef FEATURES_DIALOG_H
#define FEATURES_DIALOG_H

#include <QDialog>


namespace Ui {
class FeaturesDialog;
}

///
/// \brief Prompt for features information.
///
class FeaturesDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FeaturesDialog(QWidget *parent = nullptr);
    ~FeaturesDialog();

private:
    Ui::FeaturesDialog *ui;
};


#endif // FEATURES_DIALOG_H
