#include "base_canvas_editor.h"

#include <QPainter>


BaseCanvasEditor::BaseCanvasEditor(QWidget *parent)
    : QWidget(parent)
    , m_paintCanvas()
    , m_paintRect()
    , m_zoom(1.f)
    , m_brushSize(1)
    , m_brushColor(Qt::red) {}


QPixmap BaseCanvasEditor::paintCanvas() const { return m_paintCanvas; }


void BaseCanvasEditor::setPaintCanvas(QPixmap canvas) { m_paintCanvas = canvas; }


void BaseCanvasEditor::setPaintRect(const QRect &rect) { m_paintRect = rect; }


void BaseCanvasEditor::setZoom(float factor) { m_zoom = factor; }


void BaseCanvasEditor::setBrushSize(int size) { m_brushSize = size; }


void BaseCanvasEditor::setBrushColor(const QColor &color) { m_brushColor = color; }


QPoint BaseCanvasEditor::viewToCanvas(const QPointF &point)
{
    QPoint topLeft(std::max(0, rect().center().x() -
                            static_cast<int>(m_paintCanvas.size().width() * m_zoom / 2)),
                   std::max(0, rect().center().y() -
                            static_cast<int>(m_paintCanvas.size().height() * m_zoom / 2)));
    QRect centered(topLeft, m_paintCanvas.size() * m_zoom);

    // Account for scroll offset.
    centered.translate(m_paintRect.topLeft());

    QPoint canvasPoint(std::floor(point.x()) - centered.left(),
                       std::floor(point.y()) - centered.top());
    canvasPoint = canvasPoint / m_zoom;

    return canvasPoint;
}


void BaseCanvasEditor::paintEvent(QPaintEvent *)
{
    // Paint the canvas onto the editor.
    QPainter painter(this);

    QPixmap zoomed = m_paintCanvas.scaled(m_paintCanvas.size() * m_zoom);
    QPoint topLeft(std::max(0, rect().center().x() - zoomed.size().width() / 2),
                   std::max(0, rect().center().y() - zoomed.size().height() / 2));
    QRect centered(topLeft, zoomed.size());

    // Account for scroll offset.
    centered.translate(m_paintRect.topLeft());

    painter.drawPixmap(centered, zoomed);
}
