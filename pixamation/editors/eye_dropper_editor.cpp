#include "eye_dropper_editor.h"
#include <QMouseEvent>


EyeDropperEditor::EyeDropperEditor(QWidget *parent)
    : BaseCanvasEditor(parent)
    , m_current()
    , m_color() {}


void EyeDropperEditor::setModelIndex(const QModelIndex &index) { m_current = index; }


QColor EyeDropperEditor::colorPicked() const { return m_color; }


void EyeDropperEditor::mousePressEvent(QMouseEvent *event)
{
    QPoint canvasPoint = viewToCanvas(event->position());

    QImage image = m_current.data().value<QPixmap>().toImage();

    if (image.rect().contains(canvasPoint))
    {
        m_color = image.pixelColor(canvasPoint);
        if (m_color != Qt::transparent)
            emit editingFinished(this);
    }
}


void EyeDropperEditor::mouseMoveEvent(QMouseEvent *event)
{
    QPoint canvasPoint = viewToCanvas(event->position());

    QImage image = m_current.data().value<QPixmap>().toImage();

    if (image.rect().contains(canvasPoint))
    {
        m_color = image.pixelColor(canvasPoint);
        if (m_color != Qt::transparent)
            emit editingFinished(this);
    }
}
