#ifndef EYEDROPPEREDITOR_H
#define EYEDROPPEREDITOR_H

#include <QAbstractItemModel>
#include "editors/base_canvas_editor.h"


///
/// \brief Allows picking of color on the canvas.
///
class EyeDropperEditor : public BaseCanvasEditor
{
    Q_OBJECT
public:
    EyeDropperEditor(QWidget *parent = nullptr);

    // For knowing the current item on the canvas.
    void setModelIndex(const QModelIndex& index);

    QColor colorPicked() const;

private:
    QModelIndex m_current;
    QColor m_color;


    // QWidget interface
protected:
    virtual void mousePressEvent(QMouseEvent *event) override;
    virtual void mouseMoveEvent(QMouseEvent *event) override;
};


#endif // EYEDROPPEREDITOR_H
