#include "brush_editor.h"

#include <QPaintEvent>
#include <QPainter>
#include <QPainterPath>


BrushEditor::BrushEditor(QWidget *parent)
    : BaseCanvasEditor(parent)
    , m_isPainting(false)
    , m_prevMousePos()
    , m_nextMousePos()
    , m_paintPath()
    , m_eraseMode(false)
    , m_dirty(false) { setMouseTracking(true); }


void BrushEditor::setEraseMode(bool erase) { m_eraseMode = erase; }


bool BrushEditor::isDirty() const { return m_dirty; }


void BrushEditor::paintEvent(QPaintEvent *event)
{
    if (m_isPainting)
    {
        // Paint edits onto the canvas.
        // Edits aren't committed until editingFinished is emitted.
        QPainter painter(&m_paintCanvas);
        painter.setPen(QPen(QBrush(m_brushColor), m_brushSize));

        QPixmap oldState;
        if (m_eraseMode)
        {
            oldState = m_paintCanvas;
            painter.setCompositionMode(QPainter::CompositionMode_Clear);
        }

        QPoint canvasPoint = viewToCanvas(m_nextMousePos);
        if (m_paintCanvas.rect().contains(canvasPoint))
        {
            painter.drawPoint(canvasPoint);

            // If it's not in erase mode, it is sure to paint.
            if (!m_eraseMode && !m_dirty)
                m_dirty = true;
        }

        painter.drawPath(m_paintPath);
        m_paintPath.clear();

        // In erase mode, check pixel for pixel to see if the paint canvas is dirty.
        // Only perform this check if the paint canvas is not dirty.
        // NOTE: Method might be slow.
        if (m_eraseMode && !m_dirty && oldState.toImage() != m_paintCanvas.toImage())
            m_dirty = true;
    }

    BaseCanvasEditor::paintEvent(event);
}


void BrushEditor::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        m_isPainting = true;
        m_paintPath.clear();
        m_nextMousePos = event->position();
        m_prevMousePos = event->position();
        update();
    }
}


void BrushEditor::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        m_isPainting = false;
        // Commit data to model on mouse released.
        emit editingFinished(this);
        m_dirty = false;
        update();
    }
}


void BrushEditor::mouseMoveEvent(QMouseEvent *event)
{
    m_nextMousePos = event->position();

    if (m_isPainting)
    {
        m_paintPath.moveTo(viewToCanvas(m_prevMousePos));
        m_paintPath.lineTo(viewToCanvas(m_nextMousePos));
        update();
    }

    m_prevMousePos = event->position();
}
