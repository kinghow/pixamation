#ifndef BASECANVASEDITOR_H
#define BASECANVASEDITOR_H

#include <QWidget>


///
/// \brief A base class for editors that require canvas information such as zoom.
///
/// Call setCanvasProperties to setup.
///
class BaseCanvasEditor : public QWidget
{
    Q_OBJECT
public:
    explicit BaseCanvasEditor(QWidget *parent = nullptr);
    QPixmap paintCanvas() const;
    void setPaintCanvas(QPixmap canvas);
    void setPaintRect(const QRect& rect);

signals:
    // Emit to signal apply changes to model.
    void editingFinished(BaseCanvasEditor *editor);

public slots:
    void setZoom(float factor);
    void setBrushSize(int size);
    void setBrushColor(const QColor &color);

protected:
    // Convert view coordinates to canvas coordinates.
    QPoint viewToCanvas(const QPointF &point);

protected:
    QPixmap m_paintCanvas;

    // The current rect of the canvas including scroll offsets.
    QRect m_paintRect;

    float m_zoom;
    int m_brushSize;
    QColor m_brushColor;


    // QWidget interface
protected:
    virtual void paintEvent(QPaintEvent *) override;
};


#endif // BASECANVASEDITOR_H
