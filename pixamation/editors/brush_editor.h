#ifndef BRUSHEDITOR_H
#define BRUSHEDITOR_H

#include <QPainterPath>
#include "editors/base_canvas_editor.h"


///
/// \brief For drawing on the canvas.
///
class BrushEditor : public BaseCanvasEditor
{
    Q_OBJECT
public:
    explicit BrushEditor(QWidget *parent = nullptr);
    void setEraseMode(bool erase);
    bool isDirty() const;

private:
    bool m_isPainting;
    QPointF m_prevMousePos;
    QPointF m_nextMousePos;
    QPainterPath m_paintPath;

    bool m_eraseMode;
    bool m_dirty;


    // QWidget interface
protected:
    virtual void paintEvent(QPaintEvent *event) override;
    virtual void mousePressEvent(QMouseEvent *event) override;
    virtual void mouseReleaseEvent(QMouseEvent *event) override;
    virtual void mouseMoveEvent(QMouseEvent *event) override;
};


#endif // BRUSHEDITOR_H
