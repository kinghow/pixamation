#include "frame_seq_model.h"

#include <QMimeData>
#include <QIODevice>
#include <QImage>


FrameSeqModel::FrameSeqModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_frames()
{
    // Enforce minimum 1 row invariant.
    insertRow(0);
}


int FrameSeqModel::rowCount(const QModelIndex &) const { return m_frames.size(); }


QVariant FrameSeqModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::DisplayRole || role == Qt::EditRole)
        return m_frames.at(index.row());

    return QVariant();
}


bool FrameSeqModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;

    if (role == Qt::EditRole)
    {
        m_frames.at(index.row()) = value.value<QPixmap>();
        emit dataChanged(index, index, { role });
        return true;
    }

    return false;
}


// Inserts before row.
bool FrameSeqModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);

    for (int i = 0; i < count; ++i)
    {
        QImage image(80, 80, QImage::Format_ARGB32);
        image.fill(QColor(rand() % 255, rand() % 255, rand() % 255));

        m_frames.insert(m_frames.begin() + row, QPixmap::fromImage(image));
    }

    endInsertRows();
    return true;
}


bool FrameSeqModel::removeRows(int row, int count, const QModelIndex &parent)
{
    // Enforce minimum 1 row invariant.
    int adjustedCount = count;
    if (static_cast<std::size_t>(count) == m_frames.size())
        adjustedCount = count - 1;

    beginRemoveRows(parent, row, row + adjustedCount - 1);

    for (int i = 0; i < adjustedCount; ++i)
    {

        if (m_frames.size() == 1)
            break;

        m_frames.erase(m_frames.begin() + row);
    }

    endRemoveRows();
    return true;
}


Qt::DropActions FrameSeqModel::supportedDropActions() const
{
    return QAbstractItemModel::supportedDropActions() | Qt::MoveAction;
}


Qt::ItemFlags FrameSeqModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags defaultFlags = QAbstractListModel::flags(index);

    if (index.isValid())
        return Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | defaultFlags;
    else
        // Support dropping items anywhere within the view.
        return Qt::ItemIsDropEnabled | defaultFlags;
}


QStringList FrameSeqModel::mimeTypes() const { return { "image/ *" }; }


QMimeData *FrameSeqModel::mimeData(const QModelIndexList &indexes) const
{
    // Encode pixmap data as mime data for each index.
    QMimeData *mimeData = new QMimeData;
    QByteArray encodedData;
    QDataStream stream(&encodedData, QIODevice::WriteOnly);

    for (auto &index : indexes)
        stream << index.data().value<QPixmap>();

    mimeData->setData(mimeTypes().at(0), encodedData);
    return mimeData;
}


bool FrameSeqModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int,
                                 const QModelIndex &parent)
{
    if (action == Qt::IgnoreAction)
        return true;

    int beginRow;
    if (row != -1)
        // Data is dropped elsewhere in the view.
        beginRow = row;
    else if (parent.isValid())
        // Data is dropped onto another item.
        beginRow = parent.row();
    else
        // Data is dropped elsewhere in the view.
        beginRow = rowCount(QModelIndex());


    // Decode and setup for rows insertion.
    QByteArray encodedData = data->data(mimeTypes().at(0));
    QDataStream stream(&encodedData, QIODevice::ReadOnly);
    QList<QPixmap> newItems;
    int rows = 0;
    while (!stream.atEnd())
    {
        QPixmap pixmap;
        stream >> pixmap;
        newItems << pixmap;
        ++rows;
    }

    // Insert rows and set data.
    insertRows(beginRow, rows, QModelIndex());
    for (const QPixmap &pixmap : std::as_const(newItems))
    {
        QModelIndex i = index(beginRow, 0, QModelIndex());
        setData(i, pixmap, Qt::EditRole);
        ++beginRow;
    }

    // Dragged rows are removed when returning true.
    return true;
}
