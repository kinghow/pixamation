#include "frame_seq_canvas_proxy.h"


FrameSeqCanvasProxy::FrameSeqCanvasProxy(QObject *parent)
    : QSortFilterProxyModel(parent)
    , m_refSelectionModel()
    , m_onionBackRange(0) {}


QModelIndex FrameSeqCanvasProxy::currentIndex() const
{
    if (!m_refSelectionModel)
        return QModelIndex();

    QModelIndex index = mapFromSource(m_refSelectionModel->currentIndex());
    assert(index.isValid());
    return index;
}


void FrameSeqCanvasProxy::setRefSelectionModel(QItemSelectionModel *refSelectionModel)
{
    m_refSelectionModel = refSelectionModel;
}


int FrameSeqCanvasProxy::onionRange() const { return m_onionBackRange; }


void FrameSeqCanvasProxy::setInvalidateRows() { invalidateRowsFilter(); }


void FrameSeqCanvasProxy::setOnionRange(int range)
{
    m_onionBackRange = range;
    setInvalidateRows();
    emit onionRangedChanged();
}


Qt::ItemFlags FrameSeqCanvasProxy::flags(const QModelIndex &index) const
{
    Qt::ItemFlags defaultFlags = QSortFilterProxyModel::flags(index);

//    if (m_refSelectionModel)
    if (index.isValid())
        // Support editing.
        return Qt::ItemIsEditable | defaultFlags;

    return defaultFlags;
}


bool FrameSeqCanvasProxy::filterAcceptsRow(int source_row, const QModelIndex &) const
{
    if (m_refSelectionModel)
        if (source_row >= m_refSelectionModel->currentIndex().row() - m_onionBackRange &&
            source_row <= m_refSelectionModel->currentIndex().row())
            return true;

    return false;
}
