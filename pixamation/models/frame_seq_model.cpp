#include "frame_seq_model.h"
#include <QDropEvent>


FrameSeqModel::FrameSeqModel(const QSize &frameSize, QObject *parent)
    : QAbstractListModel(parent)
    , m_frames()
    , m_frameSize(frameSize)
{
    // Enforce minimum 1 row invariant.
    insertRow(0);
}


int FrameSeqModel::rowCount(const QModelIndex &) const { return m_frames.size(); }


QVariant FrameSeqModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::DisplayRole || role == Qt::EditRole)
        return m_frames.at(index.row());

    return QVariant();
}


bool FrameSeqModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;

    if (role == Qt::EditRole)
    {
        m_frames.replace(index.row(), value.value<QPixmap>());
        emit dataChanged(index, index, { role });
        return true;
    }

    return false;
}


// Inserts before row.
bool FrameSeqModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);

    for (int i = 0; i < count; ++i)
    {
        QImage image(m_frameSize, QImage::Format_ARGB32);
        image.fill(Qt::transparent);
//        image.fill(QColor(rand() % 255, rand() % 255, rand() % 255));

        m_frames.insert(m_frames.cbegin() + row, QPixmap::fromImage(image, Qt::NoOpaqueDetection));
    }

    endInsertRows();

    return true;
}


bool FrameSeqModel::removeRows(int row, int count, const QModelIndex &parent)
{
    // Enforce minimum 1 row invariant.
    int adjustedCount = count;
    int availableCount = static_cast<int>(m_frames.size()) - row;
    if (count >= availableCount)
        adjustedCount = m_frames.size() - row;

    if (m_frames.size() == adjustedCount)
        adjustedCount -= 1;

    beginRemoveRows(parent, row, row + adjustedCount - 1);

    for (int i = 0; i < adjustedCount; ++i)
        m_frames.erase(m_frames.cbegin() + row);

    endRemoveRows();

    return true;
}


bool FrameSeqModel::moveRows(const QModelIndex &sourceParent, int sourceRow, int count,
                             const QModelIndex &destinationParent, int destinationChild)
{
    if (sourceRow < 0
        ||sourceRow + count - 1 >= rowCount(sourceParent)
        || destinationChild < 0
        || destinationChild > rowCount(destinationParent)
        || sourceRow == destinationChild
        || sourceRow == destinationChild - 1
        || sourceParent.isValid()
        || destinationParent.isValid())
        return false;

    if (!beginMoveRows(QModelIndex(), sourceRow, sourceRow + count - 1,
                       QModelIndex(), destinationChild))
        return false;

    int fromRow = sourceRow;
    if (destinationChild < sourceRow)
        fromRow += count - 1;
    else
        --destinationChild;

    // Move in descending order so earlier items aren't affected.
    for (int i = count; i > 0; --i)
        m_frames.move(fromRow, destinationChild);

    endMoveRows();
    return true;
}


Qt::ItemFlags FrameSeqModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags defaultFlags = QAbstractListModel::flags(index);

    if (index.isValid())
        // Don't support dropping on items.
        return Qt::ItemIsDragEnabled | defaultFlags;
    else
        // Support dropping items anywhere within the view.
        return Qt::ItemIsDropEnabled | defaultFlags;
}


Qt::DropActions FrameSeqModel::supportedDropActions() const { return Qt::MoveAction; }


//QStringList FrameSeqModel::mimeTypes() const { return { "text/plain" }; }


//QMimeData *FrameSeqModel::mimeData(const QModelIndexList &indexes) const
//{
//    // Encode row data as mime data for each index.
//    QMimeData *mimeData = new QMimeData;
//    QByteArray encodedData;
//    QDataStream stream(&encodedData, QIODevice::WriteOnly);

//    for (auto &index : indexes)
//        stream << QString::number(index.row());

//    mimeData->setData(mimeTypes().at(0), encodedData);
//    return mimeData;
//}


//bool FrameSeqModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int,
//                                 const QModelIndex &)
//{
//    if (action == Qt::IgnoreAction)
//        return true;

//    int beginRow;
//    if (row != -1)
//        beginRow = row;
//    else
//        beginRow = rowCount();


//    qDebug() << "hi";

//    // Decode row data and setup for moving operation.
//    QByteArray encodedData = data->data(mimeTypes().at(0));
//    QDataStream stream(&encodedData, QIODevice::ReadOnly);
//    std::vector<int> rows;
//    while (!stream.atEnd())
//    {
//        QString row;
//        stream >> row;
//        rows.push_back(row.toInt());
//    }


//    // Move rows.
//    std::sort(rows.begin(), rows.end(), std::less<int>());
//    for (auto row : rows)
//    {
//        moveRow(QModelIndex(), row, QModelIndex(), beginRow);
//        ++beginRow;
//    }

//    return true;
//}
