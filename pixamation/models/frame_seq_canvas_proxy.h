#ifndef FRAMESEQCANVASPROXY_H
#define FRAMESEQCANVASPROXY_H

#include <QSortFilterProxyModel>
#include <QItemSelectionModel>

///
/// \brief Proxy model for filtering down to only limited items based on the current index of a
/// view. This model supports onion skinning. Connect the currentIndexChanged signal from
/// FrameSeqListView to this model's slot to update the filtering whenever the source model is
/// manipulated. This model does nothing without a reference selection model.
///
class FrameSeqCanvasProxy : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    FrameSeqCanvasProxy(QObject *parent = nullptr);
    QModelIndex currentIndex() const;
    void setRefSelectionModel(QItemSelectionModel *refSelectionModel);
    int onionRange() const;

signals:
    void onionRangedChanged();

public slots:
    void setInvalidateRows();
    void setOnionRange(int range);

private:
    QItemSelectionModel *m_refSelectionModel;
    int m_onionBackRange;


    // QAbstractItemModel interface
public:
    virtual Qt::ItemFlags flags(const QModelIndex &index) const override;

    // QSortFilterProxyModel interface
protected:
    virtual bool filterAcceptsRow(int source_row, const QModelIndex &) const override;

};


#endif // FRAMESEQCANVASPROXY_H
