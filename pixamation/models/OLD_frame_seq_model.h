#ifndef FRAMESEQMODEL_H
#define FRAMESEQMODEL_H

#include <QAbstractListModel>
#include <QPixmap>

///
/// \brief Represents a sequence of frames. This class has a minimum 1 frame invariant.
/// Inserting and removing rows do not change view current item and selections. Implement
/// current item and selections behaviour in another class and call add or delete frames
/// from there. See FrameSeqWidget.
///
/// This model supports drag and drop.
///
class FrameSeqModel : public QAbstractListModel
{
    Q_OBJECT
public:
    FrameSeqModel(QObject *parent = nullptr);

private:
    // Store everything as QPixmap. Only convert to QImage when editing.
    std::vector<QPixmap> m_frames;


    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    virtual bool insertRows(int row, int count, const QModelIndex &parent) override;
    virtual bool removeRows(int row, int count, const QModelIndex &parent) override;
    virtual Qt::DropActions supportedDropActions() const override;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const override;
    virtual QStringList mimeTypes() const override;
    virtual QMimeData *mimeData(const QModelIndexList &indexes) const override;
    virtual bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int,
                              const QModelIndex &parent) override;
};

#endif // FRAMESEQMODEL_H
