#ifndef FRAMESEQMODEL_H
#define FRAMESEQMODEL_H

#include <QAbstractListModel>
#include <QPixmap>


///
/// \brief Represents a sequence of frames. This model has a 1 item minimum invariant. This model
/// supports drag and drop.
///
class FrameSeqModel : public QAbstractListModel
{
    Q_OBJECT
public:
    FrameSeqModel(const QSize &frameSize, QObject *parent = nullptr);

private:
    // Store everything as QPixmap. Only convert to QImage when editing.
    QList<QPixmap> m_frames;
    QSize m_frameSize;


    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    virtual bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    virtual bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    virtual bool moveRows(const QModelIndex &sourceParent, int sourceRow, int count,
                          const QModelIndex &destinationParent, int destinationChild) override;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const override;
    virtual Qt::DropActions supportedDropActions() const override;
//    virtual QStringList mimeTypes() const override;
//    virtual QMimeData *mimeData(const QModelIndexList &indexes) const override;
//    virtual bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int,
//                              const QModelIndex &) override;
};


#endif // FRAMESEQMODEL_H
