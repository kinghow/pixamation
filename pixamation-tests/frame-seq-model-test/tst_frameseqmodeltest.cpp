#include <QtTest>
#include <QCoreApplication>

#include "models/frame_seq_model.h"


class FrameSeqModelTest : public QObject
{
    Q_OBJECT
private slots:
    void initTestCase() {}
    void cleanupTestCase() {}


    /// \brief The model should have at least 1 item after creation.
    void new_model_frame_count()
    {
        FrameSeqModel model({ 50, 50 });
        QCOMPARE(model.rowCount(), 1);
    }

    /// \brief The model should have frames of the specified size.
    void new_model_frame_size()
    {
        for (int i = 0; i < 50; ++i)
        {
            QSize randomSize(rand() % 1000, rand() % 1000);
            FrameSeqModel model(randomSize);
            QCOMPARE(model.index(0).data().value<QPixmap>().size(), randomSize);
        }
    }

    /// \brief A new frame should be inserted before the specified row.
    void insert_frame_single()
    {
        FrameSeqModel model({ 50, 50 });
        QPersistentModelIndex first = model.index(0);

        // Insert at the beginning.
        model.insertRow(0);
        QCOMPARE(first.row(), 1);

        // Insert at the end.
        model.insertRow(2);
        QCOMPARE(first.row(), 1);

        // Insert in the middle.
        model.insertRow(1);
        QCOMPARE(first.row(), 2);

        QCOMPARE(model.rowCount(), 4);
    }

    /// \brief The frame should be removed or maintain at least 1 item.
    void remove_frame_single()
    {
        FrameSeqModel model({ 50, 50 });
        QPersistentModelIndex first = model.index(0);

        // Remove with 1 item.
        model.removeRow(0);
        model.removeRow(0);
        QCOMPARE(first.row(), 0);

        model.insertRow(0);
        model.insertRow(0);
        model.removeRow(0);
        QCOMPARE(first.row(), 1);

        QCOMPARE(model.rowCount(), 2);
    }

    /// \brief Frames should be inserted before the specified row.
    void insert_frame_bulk()
    {
        FrameSeqModel model({ 50, 50 });
        QPersistentModelIndex first = model.index(0);

        // Insert at the beginning.
        model.insertRows(0, 10);
        QCOMPARE(model.rowCount(), 11);
        QCOMPARE(first.row(), 10);

        // Insert at the end.
        model.insertRows(11, 10);
        QCOMPARE(model.rowCount(), 21);
        QCOMPARE(first.row(), 10);
    }

    /// \brief Frames should be removed from the specified row up to the specified count,
    /// maintaining at least 1 item.
    void remove_frame_bulk()
    {
        FrameSeqModel model({ 50, 50 });
        QPersistentModelIndex first = model.index(0);
        model.insertRows(0, 50);

        // Remove normally.
        model.removeRows(6, 10);
        QCOMPARE(model.rowCount(), 41);
        QCOMPARE(first.row(), 40);

        // Remove more than available.
        model.removeRows(7, 60);
        QCOMPARE(model.rowCount(), 7);
        model.removeRows(0, 60);
        QCOMPARE(model.rowCount(), 1);
    }

    /// \brief Frame should be moved before the specified row.
    void move_frame_single()
    {
        FrameSeqModel model({ 50, 50 });
        model.insertRows(0, 10);
        QPersistentModelIndex move = model.index(3);

        model.moveRow(QModelIndex(), move.row(), QModelIndex(), 8);
        QCOMPARE(move.row(), 7);

        // Move to the beginning.
        model.moveRow(QModelIndex(), 7, QModelIndex(), 0);
        QCOMPARE(move.row(), 0);

        // Move to the end.
        model.moveRow(QModelIndex(), 0, QModelIndex(), 11);
        QCOMPARE(move.row(), 10);
    }

    /// \brief Frames should be moved before the specified row, keeping the same order.
    /// Invalid move should do nothing.
    void move_frame_bulk()
    {
        FrameSeqModel model({ 50, 50 });
        model.insertRows(0, 20);

        QPersistentModelIndex moveBegin = model.index(3);
        QPersistentModelIndex move1 = model.index(4);
        QPersistentModelIndex move2 = model.index(5);
        QPersistentModelIndex move3 = model.index(6);
        QPersistentModelIndex move4 = model.index(7);
        QPersistentModelIndex moveEnd = model.index(8);

        // Moving 6 items at once to the end.
        int count = moveEnd.row() - moveBegin.row() + 1;
        model.moveRows(QModelIndex(), moveBegin.row(), count, QModelIndex(), 21);
        QCOMPARE(moveBegin.row(), 15);
        QCOMPARE(move1.row(), 16);
        QCOMPARE(move2.row(), 17);
        QCOMPARE(move3.row(), 18);
        QCOMPARE(move4.row(), 19);
        QCOMPARE(moveEnd.row(), 20);

        // Moving 6 items at once to the beginning.
        model.moveRows(QModelIndex(), moveBegin.row(), count, QModelIndex(), 0);
        QCOMPARE(moveBegin.row(), 0);
        QCOMPARE(move1.row(), 1);
        QCOMPARE(move2.row(), 2);
        QCOMPARE(move3.row(), 3);
        QCOMPARE(move4.row(), 4);
        QCOMPARE(moveEnd.row(), 5);

        // Invalid move should do nothing.
        model.moveRows(QModelIndex(), moveBegin.row(), count, QModelIndex(), 50);
        QCOMPARE(moveBegin.row(), 0);
        QCOMPARE(move1.row(), 1);
        QCOMPARE(move2.row(), 2);
        QCOMPARE(move3.row(), 3);
        QCOMPARE(move4.row(), 4);
        QCOMPARE(moveEnd.row(), 5);
    }

    /// \brief Setting model data should result in a copy operation from the source into the model
    /// data.
    void setting_frame()
    {
        FrameSeqModel model({ 50, 50 });

        QPixmap source = QPixmap::fromImage(QImage({50, 50}, QImage::Format_ARGB32_Premultiplied));
        source.fill(Qt::yellow);

        model.setData(model.index(0), source, Qt::EditRole);
        QCOMPARE(model.index(0).data().value<QPixmap>(), source);
        QVERIFY(source.data_ptr() != nullptr);
    }
};


QTEST_MAIN(FrameSeqModelTest)

#include "tst_frameseqmodeltest.moc"
