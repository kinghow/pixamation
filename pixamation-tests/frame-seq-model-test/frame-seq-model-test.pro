QT += testlib core gui

CONFIG += c++17 qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_frameseqmodeltest.cpp

LIBS += -L$$OUT_PWD/../../bin -lpixamation

INCLUDEPATH += $$PWD/../../pixamation
