QT += testlib core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17 qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_frameseqcanvasproxytest.cpp

LIBS += -L$$OUT_PWD/../../bin -lpixamation

INCLUDEPATH += $$PWD/../../pixamation
