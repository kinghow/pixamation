#include <QtTest>
#include <QCoreApplication>

#include "models/frame_seq_canvas_proxy.h"
#include "models/frame_seq_model.h"
#include "views/frame_seq_list_view.h"


class FrameSeqCanvasProxyTest : public QObject
{
    Q_OBJECT
private slots:
    void initTestCase() {}

    void cleanupTestCase() {}


    /// \brief Should get the same value that was set.
    void set_onion_range()
    {
        FrameSeqCanvasProxy proxy;
        proxy.setOnionRange(7);

        QCOMPARE(proxy.onionRange(), 7);
    }

    /// \brief The proxy should filter down to only 1 item by default. The item should be the same
    /// as the current item on the view.
    void filter_current_view_index()
    {
        FrameSeqModel model({50, 50});
        model.insertRows(0, 20);

        FrameSeqListView view;
        view.setModel(&model);

        FrameSeqCanvasProxy proxy;
        proxy.setSourceModel(&model);
        proxy.setRefSelectionModel(view.selectionModel());

        view.setCurrentIndex(model.index(9));
        proxy.invalidate();

        QCOMPARE(proxy.mapToSource(proxy.currentIndex()), view.currentIndex());
        QCOMPARE(proxy.rowCount(), 1);
    }

    /// \brief The proxy should filter down to the specified range. If the number of available
    /// previous items are less than the range, show all.
    void filter_with_onion_range()
    {
        FrameSeqModel model({50, 50});
        model.insertRows(0, 20);

        FrameSeqListView view;
        view.setModel(&model);
        view.setCurrentIndex(model.index(0));

        FrameSeqCanvasProxy proxy;
        proxy.setSourceModel(&model);
        proxy.setRefSelectionModel(view.selectionModel());
        proxy.setOnionRange(3);

        // Show the current item + the onion range.
        view.setCurrentIndex(model.index(8));
        proxy.invalidate();
        QCOMPARE(proxy.rowCount(), 4);

        // Items less than range.
        view.setCurrentIndex(model.index(0));
        proxy.invalidate();
        QCOMPARE(proxy.rowCount(), 1);
    }
};


QTEST_MAIN(FrameSeqCanvasProxyTest)

#include "tst_frameseqcanvasproxytest.moc"
