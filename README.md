# Pixamation

## Summary

Pixamation is a 2D pixel drawing and animation program made with [Qt](https://www.qt.io/).

## Features
![general](images/features-general.png)
![edit](images/features-edit.png)
![canvas](images/features-canvas.png)
![animation](images/features-animation.png)

## Screenshot
![screenshot](images/screenshot.png)

## Sample Outputs
![Ball](images/ball.gif) ![Flame](images/flame.gif) ![Beam](images/beam.gif)

## How to Run
### Pre-built Binary
1. Run the pre-built binary in the bin folder.

### Building from Source
1. Install [QtCreator](https://www.qt.io/download-open-source) and [Qt 6.4.0](https://download.qt.io/official_releases/qt/6.4/6.4.0/).
2. Build via qmake and mingw in QtCreator and run.