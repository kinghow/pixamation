TEMPLATE = app
CONFIG += c++17
CONFIG -= app_bundle
CONFIG -= qt

DESTDIR = ../bin

SOURCES += \
        main.cpp

INCLUDEPATH += ..
LIBS += -L$$OUT_PWD/$$DESTDIR -lpixamation
