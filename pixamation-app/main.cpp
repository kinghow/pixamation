#include "pixamation/pixamation.h"
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    Pixamation p;
    return p.run(argc, argv);
}
